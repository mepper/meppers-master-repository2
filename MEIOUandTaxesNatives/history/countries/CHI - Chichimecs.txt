# CHI - Chickasaw

government = native_council
mercantilism = 0.1
primary_culture = chickasaw
religion = totemism
technology_group = north_american
capital = 3588

1000.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1356.1.1   = {
	monarch = {
		name = "Native Council"
		adm = 3
		dip = 3
		mil = 3
		regent = yes
	}
}
