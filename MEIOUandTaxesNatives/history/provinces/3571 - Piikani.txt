# No previous file for Piikani

culture = nakota
religion = totemism
capital = "Piikani"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 5
native_ferocity = 3
native_hostileness = 4

1760.1.1  = {	owner = BLA
		controller = BLA
		add_core = BLA
		trade_goods = fur
		is_city = yes } #Great Plain tribes spread over vast territories
