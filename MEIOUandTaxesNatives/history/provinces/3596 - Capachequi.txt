# No previous file for Capachequi

culture = creek
religion = totemism
capital = "Capachequi"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 5
native_ferocity = 1 
native_hostileness = 6

1650.1.1 = { owner = CRE
		controller = CRE
		is_city = yes
		add_core = CRE
		trade_goods = cotton } #Formation of the Creek confederacy
1814.8.9  = {
	owner = USA
	controller = USA
	add_core = USA
	culture = american
	religion = protestant
	trade_goods = cotton
	is_city = yes
} # Treaty of Fort Jackson ending the Creek War
