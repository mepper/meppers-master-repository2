# No previous file for Niitsitapi

culture = blackfoot
religion = totemism
capital = "Niitsitapi"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 15
native_ferocity = 3
native_hostileness = 4

1760.1.1  = {	owner = BLA
		controller = BLA
		add_core = BLA
		trade_goods = fur
		is_city = yes } #Great Plain tribes spread over vast territories
1793.1.1 = { 	
		owner = GBR
		controller = GBR
		citysize = 500
		culture = english
		religion = protestant 
		trade_goods = fur } #Fort St John, first western settlement in modern British Columbia
1818.1.1 = { add_core = GBR }
