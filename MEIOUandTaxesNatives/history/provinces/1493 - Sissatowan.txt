# No previous file for Sissatowan

culture = illini
religion = totemism
capital = "Sissatowan"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 25
native_ferocity = 2
native_hostileness = 5

1650.1.1  = {	owner = ILN
		controller = ILN
		add_core = ILN
		is_city = yes }
1670.1.1  = {  } # Robert Cavelier de La Salle
1680.1.1  = { 	owner = IRO
		controller = IRO
		citysize = 100
		culture = iroquois } #Taken by Iroquois in Beaver Wars.
1699.1.1  = {	owner = FRA
		controller = FRA
		culture = francien
		religion = catholic
		citysize = 450
		trade_goods = fur
	    } # Construction of Cahokia

1728.1.1  = { add_core = FRA }
1750.1.1  = { citysize = 980 }
1763.2.10 = {	owner = GBR
		controller = GBR
		remove_core = FRA
	    } # Treaty of Paris, France gave up its claim to New France
1763.3.1  = { unrest = 6 } # Native discontent with the British rule
1763.5.1  = { unrest = 0 revolt = { type = nationalist_rebels size = 2 } controller = REB } # Pontiac's war
1763.10.9 = {	revolt = {}
		owner = ILN
		controller = ILN
		add_core = ILN
		is_city = yes
		culture = illini
		religion = totemism
	    } # Royal proclamation, Britan recognize native lands (as protectorates)
1800.1.1  = { citysize = 2050 }
1803.7.4  = {	add_core = USA }
1813.10.5 = {	owner = USA
		controller = USA
		culture = american
		religion = protestant } #The death of Tecumseh mark the end of organized native resistance east of teh Mississippi
