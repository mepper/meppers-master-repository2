Sun_Wu's to do list for the Far East

a> for alternate history

Create system for ending Red Turban Rebellion
	Rebel Victory, Yuan are pushed north and become Mongol Khanate
	Yuan Victory, all rebels are crushed
	
Create system for dynamic death and rebirth of China
	make subsystem for randomly picking Chinese revolter tags as rebels
		Make then single use (once they enter the game, cannot be chosen again via system)

Organization of the stuff by Chinese path

Tang

Song

Han

Qin


Chinese paths

Tributary stuff
	Overlord can intervene into civil wars of tributaries
	

Zheng He
	Decide to initiate (decision)
	Setting aside money
	Building up Longjiang Shipyards
	
	First Voyage
		Champa
		Java
		Malacca
		Aru
		Semudera
		Lambri
			Excursion to Andaman and Nicobar Islands
		Ceylon
		Qiulon
		Calicut
		Fighting Chen Zuyi at Palembang
		Tribute from visited countries
		
	Second Voyage
	Third Voyage
	Fourth Voyage
	Fifth Voyage
	Sixth Voyage
	Seventh Voyage
	
	Alt-Voyages
	Eighth Voyage
	Ninth Voyage
	Tenth Voyage
	
	Ming political intervention in Indonesia and Malaysia
		Ming nurturing of Malacca
		Ming invasion of Palembang
		Ming Involvement in the Javan Civil War
			Only happens if there is a voyage in that area while there is a civil war going on.
				Intervention into a semi-generic civil war where you back a side
		Invasion of Ceylon
		Voyage to East Africa
		
Grand Canal
	Only accessible if capital is inland as purpose was to ferry grain
		Uses lots of manpower
			Gives local trade power and production

Nurgan Commission

1556 Shaanxi Earthquake

Paper Currency

Columbian Exchange
	Introduction of white potato, sweet potato and corn increase potentially arable land
		Forests cleared for crops
			Population and tax base increase
				Deforestation causes increased flooding on the Yangtze and Yellow Rivers

Silver from New World
	Starts as a trickle of money from New World giving more money, 
		eventually moves to a flood of inflation as the money supply increases massive

Tumu Crisis

Cotton
	Yangtze Delta starts with a bit of cotton production
		Over time the artisanal industries making cotton clothing expand into cloth industry
			Population in Delta increases causing people to expand outward
				Expansion causes frontiers to grow grain and former frontier to grow cotton to feed cloth industry
				

Interactions with Central Asia
	Qara Del
	Timurids
		Unification of China prompts China to send out envoys announcing the new dynasty and demanding tribute
			Timur is not amused
				Timur dies replaced by son
					Timur's son realizes neither could win war 
						decides to engage in profitable tributary trade
						a>ignores China
							a>China does nothing
							a>China invades
					a>Timur's son decides to invade
						a>Timur's son succeeds to forms new dynasty
						a>Timurids are vanquished and turned into a subject nation
				a>Timur invades
					a>Timur forms new dynasty
					a>Timurids are crushed
			a>Timur sends tribute
	Chen Cheng
		Sent if Timurids and China go for peace
			Travels around Central Asia giving events to the player giving lots of flavor information and some useful military information
	Ma Wensheng
	
Ryukyu

Fourth Chinese Domination of Vietnam

Yunnan
	Invasion
	Establishment of Native Offices
	Colonial Exploitation
	Colonial Settlement
	Replacing Chieftains with Chinese administration
	Miao Rebellions
	
Maritime Prohibitions

Wokou

Expansions into Burma

Restoration and Expansion of the Great Wall

Considering Restoration of the King of Cham

Fusion of the Gentry and Merchant classes

Work on Mandala power structures of SEA

Stuff to the West
	关西八卫 ("eight garrisons west to the pass")
		Sarig Yogir (composed of 安定，曲先 and 阿端, 3/8), inhabited Qaidam Basin
			安定, dies shortly after 1444
			阿端, ditto
			曲先, 
	Make Yumen province into another tag?
