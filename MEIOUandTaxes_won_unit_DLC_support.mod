name="M&T Wealth of Nations Western Ships DLC Support"
path="mod/MEIOUandTaxes_won_unit_DLC_support"
dependencies={
	"MEIOU and Taxes 2.02"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesWoN.jpg"
supported_version="1.24.*.*"
