crush_rome = {
	type = superiority
	
	badboy_factor = 1.0
	prestige_factor = 2
	peace_cost_factor = 0.33
	
	peace_options = {
		po_demand_provinces
		po_release_vassals
		po_release_annexed
		po_return_cores
		po_revoke_cores
		po_gold
		po_concede_defeat
	}
	allow_leader_change = no
	
	war_name = INTERVENTION_WAR_NAME
}

roman_imperialism = {
	type = superiority
	
	attacker = {
		badboy_factor = 1
		prestige_factor = 1
		peace_cost_factor = 0.75
		
		dipcost_factor = 1
		allowed_provinces = {
			always = yes
		}
		
		peace_options = {
			# # po_annex
			po_demand_provinces
			po_become_vassal
			po_gold
			po_concede_defeat
		}
		
		country_desc = ALL_COUNTRIES
		prov_desc = ALL_PROVS
	}
	
	defender = {
		badboy_factor = 0.5
		prestige_factor = 1
		peace_cost_factor = 0.75
		dipcost_factor = 0.5
		
		allowed_provinces = {
			always = yes
		}
		
		peace_options = {
			po_demand_provinces
			po_release_vassals
			po_release_annexed
			po_return_cores
			po_revoke_cores
			po_gold
			po_concede_defeat
		}
		
		country_desc = ALL_COUNTRIES
		prov_desc = ALL_PROVS
	}
	
	war_name = ROM_WAR_NAME
}

conquer_roman_core = {
	type = superiority
	
	badboy_factor = 1.0  #High AE, like Absolutism but Napoleon
	prestige_factor = 0.5
	peace_cost_factor = 0.5
	
	dipcost_factor = 1
	allowed_provinces = {
		OR = {
			region = austrian_circle_region
			region = switzerland_region
			superregion = france_superregion
			superregion = iberia_superregion
			superregion = italy_superregion
			region = west_balkan_region
			region = east_balkan_region
			region = central_balkan_region
			region = greece_region
			region = north_anatolia_region
			region = south_anatolia_region
			region = al_sham_region
			region = al_iraq_region
			region = armenia_region
			region = egypt_region
			region = west_maghreb_region
			region = central_maghreb_region
			region = east_maghreb_region
			region = south_england_region
			region = north_england_region
			area = upper_rhine_area
			area = middle_rhine_area
			area = lower_rhine_area
			area = alsace_area
			province_id = 77 # Wurtemberg
			area = upper_swabia_area
			area = upper_bavaria_area
			area = transdanubia_area
			area = alfold_area
			area = transylvania_area
			area = oltenia_area
			area = crimea_area
			province_id = 3795 # Anapa
			province_id = 1298 # Tana
			area = west_georgia_area
			area = east_georgia_area
			area = abkhazia_area
			province_id = 1317 # Shemekha
			province_id = 3810 # Salyan
			province_id = 3811 # Baku
			province_id = 424 # Shirvan
			province_id = 425 # Liege
			region = belgii_region
			province_id = 2449 # Opper-Gelre
			province_id = 96 # Zeelandt
		}
		NOT = { province_id = 1367 } #Nassau, out of the rhine border
	}
	
	allowed_provinces_are_eligible = yes
	
	peace_options = {
		po_demand_provinces
		po_gold
		po_become_vassal
	}
	allow_leader_change = no
	
	war_name = ROM_WAR_NAME
}

reclaim_rome_east = {
	type = take_province
	
	badboy_factor = 0.75
	prestige_factor = 0.5
	peace_cost_factor = 0.5
	
	allowed_provinces = {
		OR = {
			region = greece_region
			region = east_balkan_region
			region = north_anatolia_region
			region = south_anatolia_region
			area = dulkadir_area
			area = cilicia_area
			area = crimea_area
			area = albania_area
		}
	}
	
	allowed_provinces_are_eligible = yes
	
	peace_options = {
		po_demand_provinces
		# # po_annex
		po_gold
		po_concede_defeat
	}
	allow_leader_change = no
	
	war_name = ROM_EAST_WAR_NAME
}