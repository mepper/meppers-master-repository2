############################
# Canal Buildings #
############################

# Transportation
# Canal Level 1
# 
# 
# 

##################
###   Canals   ###
##################

canal_level_1 = {
	cost = 99999
	time =  36
	
	modifier = {
		
	}
	
	trigger = {
		OR = {
			can_build_canal_1 = yes
			AND = {
				can_keep_canal_1 = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = canal_level_1 # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
	}
	
	ai_will_do = {
		factor = 0
	}
}

canal_level_2 = {
	cost = 99999
	time =  36
	
	modifier = {
		
	}
	
	trigger = {
		OR = {
			can_build_canal_2 = yes
			AND = {
				can_keep_canal_2 = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = canal_level_2 # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
	}
	
	ai_will_do = {
		factor = 0
	}
}

canal_level_3 = {
	cost = 99999
	time =  36
	
	modifier = {
		
	}
	
	trigger = {
		OR = {
			can_build_canal_3 = yes
			AND = {
				can_keep_canal_3 = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = canal_level_3 # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
	}
	
	ai_will_do = {
		factor = 0
	}
}