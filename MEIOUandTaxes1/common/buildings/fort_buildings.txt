#Glossary
# cost = x #==> cost in $ to build (subject to other modifiers)
# time = x #==> number of months to build.
# modifier = m # a modifier on the province that the building gives
# trigger = t # an and trigger that needs to be fullfilled to build and keep the building
# one_per_country = yes/no # if yes, only one of these can exist in a country
# manufactory = { trade_good trade_good } # list of trade goods that get a production bonus
# onmap = yes/no # show as a sprite on the map
##################
# Fort Buildings #
##################

################################################
# Fortresses
################################################
# Fort 14th Century
# Fort 15th Century
# Fort 16th Century
# Fort 17th Century
# Fort 18th Century

################################################
# Local Fortification
################################################
# Local Fortification 1
# Local Fortification 2
# Local Fortification 3

################################################
# Tier 1, 14th Century Buildings
################################################
#########
# Forts #
#########

fort_14th = {
	cost = 10000
	time =  60
	
	trigger = {
		OR = {
			can_build_fort_14th = yes
			AND = {
				can_keep_fort_14th = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = fort_14th # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
		#owner = { total_development = 25 } #Moved to ai_will_do pending clarification on when it's supposed to do
	}
	
	modifier = {
		fort_level = 2
		local_manpower = -1.000
	}
	
	onmap = yes
	
	influencing_fort = yes
	
	ai_will_do = {
		factor = 0
		
		# Got added as a trigger with a merge, I assumed it was meant as an AI restriction / dezu
		modifier = {
			factor = 0
			NOT = { owner = { total_development = 25 } }
		}
		
		modifier = {
			factor = 0
			OR = {
				owner = { is_at_war = yes }
				owner = { is_bankrupt = yes }
				OR = {
					AND = {
						is_capital = yes
						owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 0.65 } } }
					}
					owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 0.8 } } }
				}
			}
		}
		
		modifier = {
			factor = 5
			is_capital = yes
		}
		
		modifier = {
			factor = 0.5
			NOT = {
				any_neighbor_province = {
					NOT = { owned_by = ROOT }
				}
			}
		}
		modifier = {
			factor = 2.0
			any_neighbor_province = {
				NOT = { owned_by = ROOT }
			}
		}
		modifier = {
			factor = 0.25
			is_overseas = yes
		}
		modifier = {
			factor = 0
			any_neighbor_province = {
				owned_by = ROOT
				OR = {
					has_building = fort_14th
					has_building = fort_15th
					has_building = fort_16th
					has_building = fort_17th
					has_building = fort_18th
				}
			}
		}
	}
}

################################################
# Tier 2, 15th Century Buildings
################################################
fort_15th = {
	#cost = 200
	cost = 10000
	time =  60
	
	#make_obsolete = fort_14th
	
	trigger = {
		OR = {
			can_build_fort_15th = yes
			AND = {
				can_keep_fort_15th = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = fort_15th # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
		#owner = { total_development = 35 } #Moved to ai_will_do pending clarification on when it's supposed to do
		
		
	}
	
	modifier = {
		fort_level = 4
		local_manpower = -2.000
	}
	
	onmap = yes
	
	influencing_fort = yes
	
	ai_will_do = {
		factor = 0
		
		# Got added as a trigger with a merge, I assumed it was meant as an AI restriction / dezu
		modifier = {
			factor = 0
			NOT = { owner = { total_development = 35 } }
		}
		
		modifier = {
			factor = 0
			OR = {
				owner = { is_at_war = yes }
				owner = { is_bankrupt = yes }
				OR = {
					AND = {
						is_capital = yes
						owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 0.65 } } }
					}
					owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 0.8 } } }
				}
			}
		}
		
		modifier = {
			factor = 5
			is_capital = yes
		}
		
		modifier = {
			factor = 0.5
			NOT = {
				any_neighbor_province = {
					NOT = { owned_by = ROOT }
				}
			}
		}
		modifier = {
			factor = 2.0
			any_neighbor_province = {
				NOT = { owned_by = ROOT }
			}
		}
		modifier = {
			factor = 0.25
			is_overseas = yes
		}
		modifier = {
			factor = 0
			any_neighbor_province = {
				owned_by = ROOT
				OR = {
					has_building = fort_15th
					has_building = fort_16th
					has_building = fort_17th
					has_building = fort_18th
				}
			}
		}
	}
}

################################################
# Tier 3, 16th Century Buildings
################################################
fort_16th = {
	#cost = 400
	cost = 10000
	time =  60
	
	#make_obsolete = fort_15th
	
	
	trigger = {
		OR = {
			can_build_fort_16th = yes
			AND = {
				can_keep_fort_16th = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = fort_16th # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
		#owner = { total_development = 45 } #Moved to ai_will_do pending clarification on when it's supposed to do
	}
	
	
	modifier = {
		fort_level = 6
		local_manpower = -3.000
	}
	
	onmap = yes
	
	influencing_fort = yes
	
	ai_will_do = {
		factor = 0
		
		# Got added as a trigger with a merge, I assumed it was meant as an AI restriction / dezu
		modifier = {
			factor = 0
			NOT = { owner = { total_development = 45 } }
		}
		
		modifier = {
			factor = 0
			OR = {
				owner = { is_at_war = yes }
				owner = { is_bankrupt = yes }
				OR = {
					AND = {
						is_capital = yes
						owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 0.65 } } }
					}
					owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 0.8 } } }
				}
			}
		}
		
		modifier = {
			factor = 5
			is_capital = yes
		}
		
		modifier = {
			factor = 0.5
			NOT = {
				any_neighbor_province = {
					NOT = { owned_by = ROOT }
				}
			}
		}
		modifier = {
			factor = 2.0
			any_neighbor_province = {
				NOT = { owned_by = ROOT }
			}
		}
		modifier = {
			factor = 0.25
			is_overseas = yes
		}
		modifier = {
			factor = 0
			any_neighbor_province = {
				owned_by = ROOT
				OR = {
					has_building = fort_16th
					has_building = fort_17th
					has_building = fort_18th
				}
			}
		}
	}
}

################################################
# Tier 4, 17th Century Buildings
################################################
fort_17th = {
	#cost = 800
	cost = 10000
	time =  60
	
	#make_obsolete = fort_16th
	
	trigger = {
		OR = {
			can_build_fort_17th = yes
			AND = {
				can_keep_fort_17th = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = fort_17th # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
		#owner = { total_development = 55 } #Moved to ai_will_do pending clarification on when it's supposed to do
	}
	
	modifier = {
		fort_level = 8
		local_manpower = -4.000
	}
	
	onmap = yes
	
	influencing_fort = yes
	
	ai_will_do = {
		factor = 0
		
		# Got added as a trigger with a merge, I assumed it was meant as an AI restriction / dezu
		modifier = {
			factor = 0
			NOT = { owner = { total_development = 55 } }
		}
		
		modifier = {
			factor = 0
			OR = {
				owner = { is_at_war = yes }
				owner = { is_bankrupt = yes }
				OR = {
					AND = {
						is_capital = yes
						owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 0.65 } } }
					}
					owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 0.8 } } }
				}
			}
		}
		
		modifier = {
			factor = 5
			is_capital = yes
		}
		
		modifier = {
			factor = 0.5
			NOT = {
				any_neighbor_province = {
					NOT = { owned_by = ROOT }
				}
			}
		}
		modifier = {
			factor = 2.0
			any_neighbor_province = {
				NOT = { owned_by = ROOT }
			}
		}
		modifier = {
			factor = 0.25
			is_overseas = yes
		}
		modifier = {
			factor = 0
			any_neighbor_province = {
				owned_by = ROOT
				OR = {
					has_building = fort_17th
					has_building = fort_18th
				}
			}
		}
	}
}

################################################
# Tier 5, 18th Century Buildings
################################################
fort_18th = {
	#cost = 1600
	cost = 10000
	time =  60
	
	#make_obsolete = fort_17th
	
	trigger = {
		OR = {
			can_build_fort_18th = yes
			AND = {
				can_keep_fort_18th = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = fort_18th # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
	}
	
	modifier = {
		fort_level = 10
		local_manpower = -5.000
	}
	
	onmap = yes
	
	influencing_fort = yes
	
	ai_will_do = {
		factor = 0
		
		# Got added as a trigger with a merge, I assumed it was meant as an AI restriction / dezu
		modifier = {
			factor = 0
			NOT = { owner = { total_development = 65 } }
		}
		
		modifier = {
			factor = 0
			OR = {
				owner = { is_at_war = yes }
				owner = { is_bankrupt = yes }
				OR = {
					AND = {
						is_capital = yes
						owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 0.65 } } }
					}
					owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 0.8 } } }
				}
			}
		}
		
		modifier = {
			factor = 5
			is_capital = yes
		}
		
		modifier = {
			factor = 0.5
			NOT = {
				any_neighbor_province = {
					NOT = { owned_by = ROOT }
				}
			}
		}
		modifier = {
			factor = 2.0
			any_neighbor_province = {
				NOT = { owned_by = ROOT }
			}
		}
		modifier = {
			factor = 0.25
			is_overseas = yes
		}
		modifier = {
			factor = 0
			any_neighbor_province = {
				owned_by = ROOT
				has_building = fort_18th
			}
		}
	}
}

upgrade_fort_build = {
	
	cost = 0
	time =  0
	
	trigger = {
		OR = {
			has_building = upgrade_fort_build
			OR = {
				can_build_fort_14th = yes
				can_build_fort_15th = yes
				can_build_fort_16th = yes
				can_build_fort_17th = yes
				can_build_fort_18th = yes
			}
		}
	}
	
	modifier = {
	}
	
	ai_will_do = {
		factor = 0
	}
}

################################################
# Local Fortification
################################################

local_fortification_1 = {
	cost = 10000
	time =  12
	
	trigger = {
		NOT = {
			has_building = local_fortification_2
			has_building = local_fortification_3
			
			has_building = local_fortification_2_off
			has_building = local_fortification_3_off
		}
	}
	
	modifier = {
		fort_level = 1
		#   local_defensiveness = 0.1
		local_autonomy = 0.05
		local_hostile_movement_speed = -0.2
	}
	ai_will_do = {
		factor = 0
	}
}

local_fortification_1_off = {
	cost = 10000
	time =  12
	
	trigger = {
		NOT = {
			has_building = local_fortification_2
			has_building = local_fortification_3
			
			has_building = local_fortification_2_off
			has_building = local_fortification_3_off
		}
	}
	
	modifier = {
		local_defensiveness = 0.15
		local_autonomy = 0.05
		local_hostile_movement_speed = -0.1
	}
	ai_will_do = {
		factor = 0
	}
}

local_fortification_2 = {
	cost = 10000
	time =  12
	
	trigger = {
		NOT = {
			has_building = local_fortification_3
			
			has_building = local_fortification_3_off
		}
	}
	
	modifier = {
		fort_level = 2
		#	local_defensiveness = 0.2
		local_autonomy = 0.1
		local_hostile_movement_speed = -0.4
	}
	ai_will_do = {
		factor = 0
	}
}

local_fortification_2_off = {
	cost = 10000
	time =  12
	
	trigger = {
		NOT = {
			has_building = local_fortification_3
			
			has_building = local_fortification_3_off
		}
	}
	
	modifier = {
		local_defensiveness = 0.3
		local_autonomy = 0.1
		local_hostile_movement_speed = -0.2
	}
	ai_will_do = {
		factor = 0
	}
}

local_fortification_3 = {
	cost = 10000
	time =  12
	
	trigger = {
		always = yes
	}
	
	modifier = {
		fort_level = 3
		#	local_defensiveness = 0.3
		local_autonomy = 0.2
		local_hostile_movement_speed = -0.6
	}
	ai_will_do = {
		factor = 0
	}
}

local_fortification_3_off = {
	cost = 10000
	time =  12
	
	trigger = {
		always = yes
	}
	
	modifier = {
		local_defensiveness = 0.4
		local_autonomy = 0.2
		local_hostile_movement_speed = -0.3
	}
	ai_will_do = {
		factor = 0
	}
}