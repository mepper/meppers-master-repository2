#Country Name: Please see filename.

graphical_culture = asiangfx

color = { 189  64  44 }

historical_idea_groups = {
	diplomatic_ideas
	aristocracy_ideas
	economic_ideas
	logistic_ideas
	naval_ideas
	administrative_ideas
	quantity_ideas
	spy_ideas
}

historical_units = { #Indonesian Muslim Group
	indonesian_simbalan_infantry
	indonesian_war_elephant_cavalry
	indonesian_arab_horse
	indonesian_stockade_infantry
	indonesian_heavy_elephant_cavalry
	indonesian_armored_infantry
	indonesian_tumandar_cavalry
	indonesian_arquebusier_infantry
	indonesian_tabinan_cavalry
	indonesian_mansabdar_cavalry
	indonesian_matchlockmen_infantry
	indonesian_ekanda_cavalry
	indonesian_native_musketeer_infantry
	indonesian_regular_infantry
	indonesian_bayonet_infantry
	western_carabinier_cavalry
	indonesian_drill_infantry
	western_hunter_cavalry
	indonesian_impulse_infantry
	western_lancer_cavalry
	indonesian_breech_infantry
}

monarch_names = {
	"Qizhen #0" = 100
	"Qiyu #0" = 100
	"Jianjun #0" = 100
	"Youcheng #0" = 100
	"Houzhao #0" = 100
	"Houcong #0" = 100
	"Zaihou #0" = 100
	"Yijun #0" = 100
	"Changluo #0" = 100
	"Youjian #0" = 100
	"Yousong #0" = 100
	"Yujian #0" = 100
	"Youlang #0" = 100
	"Chenggong #0" = 100
	"Kehshuan #0" = 100
	"Biao #0" = 100
	"Shuang #0" = 100
	"Gang #0" = 100
	"Su #0" = 100
	"Zhen #0" = 100
	"Fu #0" = 100
	"Zi #0" = 100
	"Qi #0" = 100
	"Tan #0" = 100
	"Chun #0" = 100
	"Bai #0" = 100
	"Gui #0" = 100
	"Zhi #0" = 100
	"Quan #0" = 100
	"Pian #0" = 100
	"Hui #0" = 100
	"Song #0" = 100
	"Mo #0" = 100
	"Ying #0" = 100
	"Jing #0" = 100
	"Dang #0" = 100
	"Nan #0" = 100
	"Gaoxu #0" = 100
	"Caosui #0" = 100
	"Gauxi #0" = 100
	"Youyuan #0" = 100
	"Youlun #0" = 100
	"Youbin #0" = 100
	"Houwei #0" = 100
	"Youxiao #0" = 100
	"Youji #0" = 100
	"Youxue #0" = 100
	"Youyi #0" = 100
	"Youshan #0" = 100
	"Cixuan #0" = 100
	"Cijiang #0" = 100
	"Cizhao #0" = 100
	"Cihuan #0" = 100
	"Cican #0" = 100
	"He #0" = 1
	"Ji #0" = 1
	"Da #0" = 1
	"Yuchun #0" = 1
	
	"Intan #0" = -1
	"Nurul #0" = -1
	"Dewi #0" = -1
	"Gita #0" = -1
	"Shinta #0" = -1
	"Dinda #0" = -1
	"Kezia #0" = -1
	"Aisyah #0" = -1
	"Rani #0" = -1
	"Diah #0" = -1
}

leader_names = {
	Li Wang Zhang Liu Zhao Chen Yang Wu Huang Zhou Xu Zhu Lin Sun Ma
	Gao Hu Zheng Guodao Xie He Xu Song Shen Luo Han Deng Liang Ye
	Peng Fang Wang Cui Cai Cheng Yuan  Pan Lu Cao Tang Feng Qian Du
}

ship_names = {
	Gaodi Huidi Wendi Zhaodi Zhangdi "Da Ming Guo"
	Beijing "Zeng He" Ji'nan Nanjing Hangzhou
	Wuchang Fuzhou Nanchang Guilin Guangzhou
	Guiyang Yunnan Chendo Xi'an Taiyuan "Huang He"
	"Chang Jiang" "Xijiang" "Kung-fu tzu" Mencius
	"Xun Zi" "Yan Hui" "Min Sun" "Ran Geng" "Ran Yong"
	"Zi-you" "Zi-lu" "Zi-wo" "Zi-gong" "Yan Yan"
	"Zi-xia" "Zi-zhang" "Zeng Shen" "Dantai Mieming"
	"Fu Buji" "Yuan Xian" "Gungye Chang" "Nangong Guo"
	"Gongxi Ai" "Zeng Dian" "Yan Wuyao" "Shang Zhu"
	"Gao Chai" "Qidiao Kai" "Gongbo Liao" "Sima Geng"
	"Fan Xu" "You Ruo" "Gongxi Chi" "Wuma Shi" "Liang Zhan"
	"Yan Xing" "Ran Ru" "Cao Xu" "Bo Qian" "Gongsun Long"
}