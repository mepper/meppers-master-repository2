#Country Name: Hausa
#Tag: HAU
#MEIOU-FB - Sudan tech group Nov 08
#dharper's African cavalry added

graphical_culture = africangfx

color = { 48  169  96 }

historical_idea_groups = {
	trade_ideas
	leadership_ideas
	administrative_ideas
	quality_ideas
	aristocracy_ideas
	spy_ideas
	diplomatic_ideas
	logistic_ideas
}

historical_units = { #Pastoralists and Coastal states
	sudanese_tuareg_camelry
	sudanese_archer_infantry
	sudanese_tribal_raider_light_cavalry
	sudanese_sword_infantry
	sudanese_war_raider_light_cavalry
	sudanese_sofa_infantry
	sudanese_pony_archer_cavalry
	sudanese_sofa_musketeer_infantry
	sudanese_angola_gun_infantry
	sudanese_eso_heavy_cavalry
	sudanese_gold_coast_infantry
	sudanese_musketeer_infantry
	sudanese_carabiner_cavalry
	sudanese_countermarch_musketeer_infantry
	sudanese_sofa_rifle_infantry
	sudanese_rifled_infantry
	sudanese_hunter_cavalry
	sudanese_impulse_infantry
	sudanese_lancer_cavalry
	sudanese_breech_infantry
}

monarch_names = {
	"Chiroma #0" = 20
	"Yakubu #0" = 20
	"Muhammad #0" = 220
	"'Abdullahi #1" = 20
	"Da'uda Abasama #0" = 40
	"Abu Bakr Kado #0" = 20
	"Yakufu #0" = 20
	"Kutambi #0" = 20
	"Alhaji #0" = 20
	"Shekkarau #1" = 20
	"Soyaki #0" = 20
	"Bawa #0" = 20
	"Babba Zaki #0" = 20
	"Dalla Gungum #0" = 1
	"Dalla Dawaki #0" = 1
	"Akal #0" = 1
	"Bachiri #0" = 1
	"Gumsara #0" = 1
	"Bardandoma #0" = 1
	"Soba #0" = 1
	"Nyakum #0" = 1
	"Yakub #0" = 1
	"Salihu #0" = 1
	"Janhazo #0" = 1
	"Karya Giwa #0" = 1
	"Abd al-Karim #0" = 1
	"Tsagarana #0" = 1
	"Agwaragwi #0" = 1
	"Magajin #0" = 1
	"Tomo #0" = 1
	"Sumail #0" = 1
	"Suleyman #0" = 1
	"Fati #0" = 1
	"Ibrahim #0" = 1
	"'Aliyu #0" = 1
	"Bako #0" = 1
	"Burema #0" = 1
	"Dudufani #0" = 1
	"Burum #0" = 1
	
	"Hude #0" = -1
	"Amoako #0" = -1
	"Pokou #0" = -1
	"Emose #0" = -1
	"Amina #0" = -1
	"Lingeer #0" = -1
	"Orrorro #0" = -1
	"Saraounia #0" = -1
}

leader_names = {
	Saibou
	Bakara
	Mamami
	Kountch�
	Diore
	Balewa
	Aluko
	Bandele
	Kanu
	Kuti
}

ship_names = {
	iska sama hazo gajimare "babban dutse"
	teku rairayi rana wata tauraro ruwa
	itace saiwa fure
}
