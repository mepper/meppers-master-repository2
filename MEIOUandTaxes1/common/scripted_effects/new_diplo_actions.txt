# Needed for new diplo actions, pending Paradox fixes the feature.
ai_aceptance_calculation_from = {
	set_variable = { which = ai_value value = 0 }
	set_variable = { which = target_trust value = 0 }
	set_variable = { which = target_opinion value = 0 }
	set_variable = { which = target_dip_rep value = 0 }
	set_variable = { which = sender_dip_rep value = 0 }
	set_variable = { which = target_legitimacy value = 0 }
	set_variable = { which = sender_legitimacy value = 0 }
	set_variable = { which = target_prestige value = 0 }
	set_variable = { which = sender_prestige value = 0 }
	set_variable = { which = target_army value = 0 }
	set_variable = { which = sender_army value = 0 }
	set_variable = { which = target_dev value = 0 }
	set_variable = { which = sender_dev value = 0 }
	set_variable = { which = target_distance value = 0 }

	export_to_variable = {
		variable_name = target_trust
		value = trust
		who = FROM
		with = ROOT
	}
	subtract_variable = { which = target_trust value = 50 }
	multiply_variable = { which = target_trust value = 2 }

	export_to_variable = {
		variable_name = target_opinion
		value = opinion
		who = FROM
		with = ROOT
	}
	divide_variable = { which = target_opinion value = 10 }			# was 4

#	export_to_variable = {
#		variable_name = target_dip_rep
#		value = modifier:diplomatic_reputation
#	}
#	multiply_variable = { which = target_dip_rep value = -2 }

#	export_to_variable = {
#		variable_name = sender_dip_rep
#		value = modifier:diplomatic_reputation
#		who = FROM
#	}
#	multiply_variable = { which = sender_dip_rep value = 2 }

	FROM = {
		set_variable = {
			which = sender_legitimacy
			which = legitimacy
		}
	}
	set_variable = {
		which = sender_legitimacy
		which = FROM
	}
	FROM = {
		set_variable = { which = sender_legitimacy value = 0 }
	}

	set_variable = {
		which = target_reptrad
		which = republican_tradition
	}
	multiply_variable = { which = target_reptrad value = -1 }

	FROM = {
		set_variable = {
			which = sender_reptrad
			which = republican_tradition
		}
	}
	set_variable = {
		which = sender_reptrad
		which = FROM
	}
	FROM = {
		set_variable = { which = sender_reptrad value = 0 }
	}

	set_variable = {
		which = target_devotion
		which = devotion
	}
	multiply_variable = { which = target_devotion value = -1 }

	FROM = {
		set_variable = {
			which = sender_devotion
			which = devotion
		}
	}
	set_variable = {
		which = sender_devotion
		which = FROM
	}
	FROM = {
		set_variable = { which = sender_devotion value = 0 }
	}

	set_variable = {
		which = target_horde_unity
		which = horde_unity
	}
	multiply_variable = { which = target_horde_unity value = -1 }

	FROM = {
		set_variable = {
			which = sender_horde_unity
			which = horde_unity
		}
	}
	set_variable = {
		which = sender_horde_unity
		which = FROM
	}
	FROM = {
		set_variable = { which = sender_horde_unity value = 0 }
	}

	export_to_variable = {
		variable_name = target_prestige
		value = prestige
	}
	multiply_variable = { which = target_prestige value = -1 }

	export_to_variable = {
		variable_name = sender_prestige
		value = prestige
		who = FROM
	}

	export_to_variable = {
		variable_name = target_army
		value = army_size
	}
	multiply_variable = { which = target_army value = -1 }
	
	export_to_variable = {
		variable_name = sender_army
		value = army_size
		who = FROM
	}
	
	export_to_variable = {
		variable_name = target_dev
		value = total_development
	}
	multiply_variable = { which = target_dev value = -1 }

	export_to_variable = {
		variable_name = sender_dev
		value = total_development
		who = FROM
	}
	divide_variable = { which = sender_dev value = 5 }

	export_to_variable = {
		which = target_distance
		value = border_distance
		who = FROM
		with = ROOT
	}
	divide_variable = { which = target_distance value = -1000 }
	
	change_variable = { which = ai_value which = target_trust }
	change_variable = { which = ai_value which = target_opinion }
	change_variable = { which = ai_value which = target_dip_rep }
	change_variable = { which = ai_value which = sender_dip_rep }
	change_variable = { which = ai_value which = target_legitimacy }
	change_variable = { which = ai_value which = sender_legitimacy }
	change_variable = { which = ai_value which = target_reptrad }
	change_variable = { which = ai_value which = sender_reptrad }
	change_variable = { which = ai_value which = target_devotion }
	change_variable = { which = ai_value which = sender_devotion }
	change_variable = { which = ai_value which = target_horde_unity }
	change_variable = { which = ai_value which = sender_horde_unity }
	change_variable = { which = ai_value which = target_prestige }
	change_variable = { which = ai_value which = sender_prestige }
	change_variable = { which = ai_value which = target_army }
	change_variable = { which = ai_value which = sender_army }
	change_variable = { which = ai_value which = target_dev }
	change_variable = { which = ai_value which = sender_dev }
	change_variable = { which = ai_value which = target_distance }

	set_variable = { which = target_trust value = 0 }
	set_variable = { which = target_opinion value = 0 }
	set_variable = { which = target_dip_rep value = 0 }
	set_variable = { which = sender_dip_rep value = 0 }
	set_variable = { which = target_legitimacy value = 0 }
	set_variable = { which = sender_legitimacy value = 0 }
	set_variable = { which = target_reptrad value = 0 }
	set_variable = { which = sender_reptrad value = 0 }
	set_variable = { which = target_devotion value = 0 }
	set_variable = { which = sender_devotion value = 0 }
	set_variable = { which = target_horde_unity value = 0 }
	set_variable = { which = sender_horde_unity value = 0 }
	set_variable = { which = target_prestige value = 0 }
	set_variable = { which = sender_prestige value = 0 }
	set_variable = { which = target_army value = 0 }
	set_variable = { which = sender_army value = 0 }
	set_variable = { which = target_dev value = 0 }
	set_variable = { which = sender_dev value = 0 }
	set_variable = { which = target_distance value = 0 }
	
	set_variable = { which = ai_likelyhood which = ai_value }
	change_variable = { which = ai_likelyhood value = 100 }
	divide_variable = { which = ai_likelyhood value = 2 }
		
	if = {
		limit = {
			is_subject_of_type = appanage_subject
			FROM = { tag = FRA NOT = { has_country_flag = edit_de_moulins } }
		}
		set_variable = { which = ai_likelyhood value = 0.01 }
		set_variable = { which = ai_value value = -1000 }
	}
	if = {
		limit = { NOT = { check_variable = { which = ai_likelyhood value = 0.01 } } }
		set_variable = { which = ai_likelyhood value = 0.01 }
	}
	if = {
		limit = { check_variable = { which = ai_likelyhood value = 100 } }
		set_variable = { which = ai_likelyhood value = 100 }
	}
}

ai_aceptance_calculation_prev = {
	set_variable = { which = ai_value value = 0 }
	set_variable = { which = target_trust value = 0 }
	set_variable = { which = target_opinion value = 0 }
	set_variable = { which = target_dip_rep value = 0 }
	set_variable = { which = sender_dip_rep value = 0 }
	set_variable = { which = target_legitimacy value = 0 }
	set_variable = { which = sender_legitimacy value = 0 }
	set_variable = { which = target_reptrad value = 0 }
	set_variable = { which = sender_reptrad value = 0 }
	set_variable = { which = target_devotion value = 0 }
	set_variable = { which = sender_devotion value = 0 }
	set_variable = { which = target_horde_unity value = 0 }
	set_variable = { which = sender_horde_unity value = 0 }
	set_variable = { which = target_prestige value = 0 }
	set_variable = { which = sender_prestige value = 0 }
	set_variable = { which = target_army value = 0 }
	set_variable = { which = sender_army value = 0 }
	set_variable = { which = target_dev value = 0 }
	set_variable = { which = sender_dev value = 0 }
	set_variable = { which = target_distance value = 0 }

	export_to_variable = {
		variable_name = target_trust
		value = trust
		who = PREV
		with = THIS
	}
	subtract_variable = { which = target_trust value = 50 }
	multiply_variable = { which = target_trust value = 2 }

	export_to_variable = {
		variable_name = target_opinion
		value = opinion
		who = PREV
		with = THIS
	}
	divide_variable = { which = target_opinion value = 10 }			# was 4

#	export_to_variable = {
#		variable_name = target_dip_rep
#		value = modifier:diplomatic_reputation
#	}
#	multiply_variable = { which = target_dip_rep value = -2 }

#	set_variable = {
#		variable_name = sender_dip_rep
#		value = modifier:diplomatic_reputation
#		who = PREV
#	}
#	multiply_variable = { which = sender_dip_rep value = 2 }

	set_variable = {
		which = target_legitimacy
		which = legitimacy
	}
	multiply_variable = { which = target_legitimacy value = -1 }

	PREV = {
		set_variable = {
			which = sender_legitimacy
			which = legitimacy
		}
	}
	set_variable = {
		which = sender_legitimacy
		which = PREV
	}
	PREV = {
		set_variable = { which = sender_legitimacy value = 0 }
	}

	set_variable = {
		which = target_reptrad
		which = republican_tradition
	}
	multiply_variable = { which = target_reptrad value = -1 }

	PREV = {
		set_variable = {
			which = sender_reptrad
			which = republican_tradition
		}
	}
	set_variable = {
		which = sender_reptrad
		which = PREV
	}
	PREV = {
		set_variable = { which = sender_reptrad value = 0 }
	}

	set_variable = {
		which = target_devotion
		which = devotion
	}
	multiply_variable = { which = target_devotion value = -1 }

	PREV = {
		set_variable = {
			which = sender_devotion
			which = devotion
		}
	}
	set_variable = {
		which = sender_devotion
		which = PREV
	}
	PREV = {
		set_variable = { which = sender_devotion value = 0 }
	}

	set_variable = {
		which = target_horde_unity
		which = horde_unity
	}
	multiply_variable = { which = target_horde_unity value = -1 }

	PREV = {
		set_variable = {
			which = sender_horde_unity
			which = horde_unity
		}
	}
	set_variable = {
		which = sender_horde_unity
		which = PREV
	}
	PREV = {
		set_variable = { which = sender_horde_unity value = 0 }
	}

	export_to_variable = {
		variable_name = target_prestige
		value = prestige
	}
	multiply_variable = { which = target_prestige value = -1 }

	export_to_variable = {
		variable_name = sender_prestige
		value = prestige
		who = PREV
	}

	export_to_variable = {
		variable_name = target_army
		value = army_size
	}
	multiply_variable = { which = target_army value = -1 }
	
	export_to_variable = {
		variable_name = sender_army
		value = army_size
		who = PREV
	}
	
	export_to_variable = {
		variable_name = target_dev
		value = total_development
	}
	multiply_variable = { which = target_dev value = -1 }

	export_to_variable = {
		variable_name = sender_dev
		value = total_development
		who = PREV
	}
	divide_variable = { which = sender_dev value = 5 }

	export_to_variable = {
		which = target_distance
		value = border_distance
		who = PREV
		with = THIS
	}
	divide_variable = { which = target_distance value = -1000 }
	
	change_variable = { which = ai_value which = target_trust }
	change_variable = { which = ai_value which = target_opinion }
	change_variable = { which = ai_value which = target_dip_rep }
	change_variable = { which = ai_value which = sender_dip_rep }
	change_variable = { which = ai_value which = target_legitimacy }
	change_variable = { which = ai_value which = sender_legitimacy }
	change_variable = { which = ai_value which = target_reptrad }
	change_variable = { which = ai_value which = sender_reptrad }
	change_variable = { which = ai_value which = target_devotion }
	change_variable = { which = ai_value which = sender_devotion }
	change_variable = { which = ai_value which = target_horde_unity }
	change_variable = { which = ai_value which = sender_horde_unity }
	change_variable = { which = ai_value which = target_prestige }
	change_variable = { which = ai_value which = sender_prestige }
	change_variable = { which = ai_value which = target_army }
	change_variable = { which = ai_value which = sender_army }
	change_variable = { which = ai_value which = target_dev }
	change_variable = { which = ai_value which = sender_dev }
	change_variable = { which = ai_value which = target_distance }

	set_variable = { which = target_trust value = 0 }
	set_variable = { which = target_opinion value = 0 }
	set_variable = { which = target_dip_rep value = 0 }
	set_variable = { which = sender_dip_rep value = 0 }
	set_variable = { which = target_legitimacy value = 0 }
	set_variable = { which = sender_legitimacy value = 0 }
	set_variable = { which = target_reptrad value = 0 }
	set_variable = { which = sender_reptrad value = 0 }
	set_variable = { which = target_devotion value = 0 }
	set_variable = { which = sender_devotion value = 0 }
	set_variable = { which = target_horde_unity value = 0 }
	set_variable = { which = sender_horde_unity value = 0 }
	set_variable = { which = target_prestige value = 0 }
	set_variable = { which = sender_prestige value = 0 }
	set_variable = { which = target_army value = 0 }
	set_variable = { which = sender_army value = 0 }
	set_variable = { which = target_dev value = 0 }
	set_variable = { which = sender_dev value = 0 }
	set_variable = { which = target_distance value = 0 }
	
	set_variable = { which = ai_likelyhood which = ai_value }
	change_variable = { which = ai_likelyhood value = 100 }
	divide_variable = { which = ai_likelyhood value = 2 }
	
	if = {
		limit = {
			is_subject_of_type = appanage_subject
			PREV = { tag = FRA NOT = { has_country_flag = edit_de_moulins } }
		}
		set_variable = { which = ai_likelyhood value = 0.01 }
		set_variable = { which = ai_value value = -1000 }
	}
	if = {
		limit = { NOT = { check_variable = { which = ai_likelyhood value = 0.01 } } }
		set_variable = { which = ai_likelyhood value = 0.01 }
	}
	if = {
		limit = { check_variable = { which = ai_likelyhood value = 100 } }
		set_variable = { which = ai_likelyhood value = 100 }
	}
}
