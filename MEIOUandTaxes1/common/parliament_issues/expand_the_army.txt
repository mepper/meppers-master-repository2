expand_the_army = {
	
	category = 1
	
	allow = {
		always = yes
	}
	
	effect = {
		
	}
	
	modifier = {
		global_regiment_recruit_speed = -0.2
		land_maintenance_modifier = -0.1
		army_tradition = 0.1
	}
	
	ai_will_do = {
		factor = 1
	}
}