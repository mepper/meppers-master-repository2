#Light Cossacks - Lisowczycy (26)

unit_type = eastern
type = cavalry
maneuver = 2

offensive_morale = 5
defensive_morale = 2
offensive_fire = 4
defensive_fire = 2
offensive_shock = 4 #BONUS
defensive_shock = 3

trigger = {
	primary_culture = polish
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}


