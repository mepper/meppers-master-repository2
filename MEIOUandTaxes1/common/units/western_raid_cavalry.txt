# Western Knights (0)

type = cavalry
unit_type = western
maneuver = 2

offensive_morale = 2
defensive_morale = 2
offensive_fire = 0
defensive_fire = 0
offensive_shock = 1
defensive_shock = 1

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}
