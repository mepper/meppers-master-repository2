# 3946 - Kozuke

owner = UTN
controller = UTN
add_core = UTN

capital = "Takasaki"
trade_goods = hemp
culture = kanto
religion = mahayana

hre = no

base_tax = 16
base_production = 0
base_manpower = 1

is_city = yes

discovered_by = chinese

1356.1.1 = {
	add_core = USG
}
1363.1.1 = {
	controller = USG
	owner = USG
}
1501.1.1 = {
	base_tax = 27
	base_production = 1
	base_manpower = 2
	small_university = yes #Ashikaga Gakko
}
1600.9.15 = {
	owner = TGW
	controller = TGW
} # Battle of Sekigahara
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
