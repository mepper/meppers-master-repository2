# 187 Barrois - Principal cities: Bar le duc , longwy

owner = BAR	# LOR
controller = BAR # LOR
add_core = BAR

capital = "Bar"
trade_goods = wheat #linen
culture = lorrain
religion = catholic

hre = yes

base_tax = 12
base_production = 1
base_manpower = 1

is_city = yes
road_network = yes
town_hall = yes
workshop = yes
# La Notre-Dame de Verdun

discovered_by = eastern
discovered_by = western
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
}
1419.3.20 = {
	owner = LOR
	controller = LOR
	add_core = LOR
}
1467.6.15 = {
	add_core = BUR
} # Charles the Bold ascends and lays claims on Lorraine
1475.11.30 = {
	owner = BUR
	controller = BUR
	unrest = 5
} # Charles the Bold annexes Lorraine
1476.1.1 = {
	controller = REB
} # Lorraine revolts against Charles the Bold during the Burgundian-Swiss War
1477.1.5 = {
	owner = LOR
	controller = LOR
	remove_core = BUR
	unrest = 0
} # Charles the Bold dies and Lorraine is re-established
#1500.1.1 = {
#	road_network = yes
#}
1520.5.5 = {
	base_tax = 14
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1535.1.1 = {
	fort_14th = yes
}
1551.3.20 = {
	controller = FRA
} # Franco-Habsburg War (1551-1559): France takes Toul & Verdun
1559.4.3 = {
	controller = BAR
} # Peace of Cateau-Cambrésis
1610.1.1 = {
	fort_14th = no
	fort_15th = yes
}
1634.1.1 = {
	controller = FRA
} # To France
1636.8.15 = {
	controller = BAR
} # Habsburg forces ravage North Eastern France, Lorraine liberated
1636.10.20 = {
	controller = FRA
} # Bernhard of Saxe-Weimar defeats the invaders and gradually pushes them back
1641.1.1 = {
	controller = BAR
} # Charles III restored
1648.10.24 = {
	hre = no
} # Treaty of Westphalia, ending the Thirty Years' War
1660.1.1 = {
	fort_15th = no
	fort_16th = yes
}
1670.1.1 = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # France back in control - Louis XIV lays claims through the Chambers of Reunion
1674.1.1 = {
	fort_16th = no
	fort_17th = yes
} # Vauban's 'pointed' fort in Toul
1679.9.20 = {
	owner = BAR
	controller = BAR
} # Treaty of Rijswijck (End of the War of the Grand Alliance): Lorraine restored
1702.1.1 = {
	controller = FRA
} # Back to France *yawn* in the Spanish War of Succession
1714.1.1 = {
	controller = BAR
} # Leopold restored when the Spanish Succession Crisis comes to an end
1766.2.23 = {
	owner = FRA
	controller = FRA
} # Death of Duke Stanislaus, Lorraine ceases to exist
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
