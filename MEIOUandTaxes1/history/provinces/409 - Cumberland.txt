#247 - Cumbria

owner = ENG
controller = ENG
add_core = ENG

capital = "Carlisle"
trade_goods = fish
culture = northern_e
religion = catholic

hre = no

base_tax = 9
base_production = 0
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes
local_fortification_1 = yes
fort_14th = yes
temple = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

450.1.1 = {
	add_permanent_province_modifier = {
		name = "marches_of_scotland"
		duration = -1
	}
}
1066.1.1 = {
	add_permanent_province_modifier = {
		name = "north_of_england"
		duration = -1
	}
}
1356.1.1 = {
	add_core = NOL
}
1453.1.1 = {
	unrest = 5
} # Start of the War of the Roses
1461.6.1 = {
	unrest = 2
} # Coronation of Edward IV
1467.1.1 = {
	unrest = 5
} # Rivalry between Edward IV & Warwick
1471.1.1 = {
	unrest = 8
} # Unpopularity of Warwick & War with Burgundy
1471.4.11 = {
	remove_province_modifier = "north_of_england"
	add_permanent_province_modifier = {
		name = "council_of_north"
		duration = -1
	}
} # Council established by Edward IV and headquartered at Sheriff Hutton Castle and Sandal Castle
1471.5.4 = {
	unrest = 2
} # Murder of Henry VI & Restoration of Edward IV
1483.6.26 = {
	unrest = 8
} # Revulsion at Supposed Murder of the Princes in the Tower
1485.8.23 = {
	unrest = 0
} # Battle of Bosworth Field & the End of the War of the Roses
1510.5.5 = {
	base_tax = 13
	base_production = 0
	base_manpower = 0
}
1530.1.1 = {
	culture = english
	road_network = no
	paved_road_network = yes
}
1585.1.1 = {
	religion = protestant #anglican
}
#1600.1.1 = {
#	fort_14th = yes
#}
1603.3.24 = {
	remove_province_modifier = "marches_of_scotland"
}
1641.11.22 = {
	remove_province_modifier = "council_of_north"
} # Council abolishedbecause of its support for Catholic Recusants
1645.6.28 = {
	controller = SCO
}
1646.5.5 = {
	controller = ENG
} # End of First English Civil War
1648.4.1 = {
	controller = REB
} # Estimated
1648.8.25 = {
	controller = ENG
}
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}