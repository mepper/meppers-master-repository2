# 3358 - Inaba

owner = YMN
controller = YMN
add_core = YMN

capital = "Tottori"
trade_goods = rice
culture = chugoku
religion = mahayana

hre = no

base_tax = 5
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = chinese

1501.1.1 = {
	base_tax = 6
	base_production = 3
	base_manpower = 1
}
1542.1.1 = {
	discovered_by = POR
}
1550.1.1 = {
	add_core = ANG
	controller = ANG
	owner = ANG
}
1582.1.1 = {
	owner = ODA
	controller = ODA
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
