# 240 - Midlands

owner = ENG
controller = ENG
add_core = ENG

capital = "Coventry"
trade_goods = wool
culture = english
religion = catholic

hre = no

base_tax = 6
base_production = 1
base_manpower = 0

is_city = yes
town_hall = yes
workshop = yes
marketplace = yes
local_fortification_1 = yes
temple = yes
road_network = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_metalwork
		duration = -1
	}
}
1453.1.1 = {
	unrest = 5
} # Start of the War of the Roses
1461.3.1 = {
	controller = REB
} # Coventry's Switch to the Yorkists
1461.6.1 = {
	unrest = 2
	controller = ENG
} # Coronation of Edward IV
1467.1.1 = {
	unrest = 5
} # Rivalry between Edward IV & Warwick
1470.9.1 = {
	controller = REB
}
1470.10.6 = {
	controller = ENG
} # Readeption of Henry VI
1471.1.1 = {
	unrest = 8
} # Unpopularity of Warwick & War with Burgundy
1471.3.15 = {
	controller = REB
} # Capture of Warwick Castle
1471.5.4 = {
	unrest = 2
	controller = ENG
} # Murder of Henry VI & Restoration of Edward IV
1483.6.26 = {
	unrest = 8
} # Revulsion at Supposed Murder of the Princes in the Tower
1485.8.23 = {
	unrest = 0
} # Battle of Bosworth Field & the End of the War of the Roses
1529.2.5 = {
	road_network = no
	paved_road_network = yes
}
1529.6.6 = {
	base_tax = 10
	base_production = 1
	base_manpower = 0
}
1548.1.1 = {
	religion = protestant
} # anglican
1585.1.1 = {
	religion = reformed
}
1600.1.1 = {
	fort_14th = yes
}
1645.12.17 = {
	controller = REB
}
1646.5.5 = {
	controller = ENG
} # End of First English Civil War
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
