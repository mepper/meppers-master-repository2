# 1110 - Bawol

capital = "Saly"
trade_goods = slaves # slaves
culture = wolof
religion = west_african_pagan_reformed

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 50
native_ferocity = 4.5
native_hostileness = 9

discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 9 }
	set_province_flag = has_estuary
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "gabu_natural_harbour"
		duration = -1
	}
}
1356.1.1 = {
	add_permanent_province_modifier = {
		name = trading_post_province
		duration = -1
	}
}
1445.1.1 = {
	discovered_by = POR
}
1536.1.1 = {
	owner = POR
	controller = POR
	add_core = POR
	citysize = 200
	trade_goods = slaves
	rename_capital = "Sali Portugal"
	change_province_name = "Casamanša"
}
