# 375 - South Munster

owner = MEA
controller = MEA
add_core = MEA

capital = "Corcaigh"
trade_goods = livestock
culture = irish
religion = catholic

hre = no

base_tax = 8
base_production = 0
base_manpower = 1

is_city = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_great_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "munster_large_natural_harbor"
		duration = -1
	}
}
700.1.1 = {
	add_permanent_province_modifier = {
		name = clan_land
		duration = -1
	}
}
1520.5.5 = {
	base_tax = 12
	base_production = 0
	base_manpower = 1
}
1583.11.11 = {
	owner = ENG
	controller = ENG
	add_core = ENG
} # End of the Kingdom of Desmond
1642.1.1 = {
	controller = REB
} # Estimated
1642.6.7 = {
	owner = IRE
	controller = IRE
} # Confederation of Kilkenny
1650.4.10 = {
	controller = ENG
} # Battle of Macroom
1652.4.1 = {
	owner = ENG
} # End of the Irish Confederates
1655.1.1 = {
	fort_14th = yes
}
1689.3.12 = {
	controller = REB
} # James II Lands in Ireland
1690.9.15 = {
	controller = ENG
}
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
