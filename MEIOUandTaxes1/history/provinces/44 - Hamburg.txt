#44 - Hamburg

owner = HAM
controller = HAM
add_core = HAM

capital = "Hamburg"
trade_goods = fish # beer
culture = old_saxon
religion = catholic

hre = yes

base_tax = 3
base_production = 2
base_manpower = 0

is_city = yes
local_fortification_1 = yes
harbour_infrastructure_2 = yes
marketplace = yes
road_network = yes
town_hall = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

#500.1.1 = {
#	add_permanent_province_modifier = {
#		name = urban_goods_beer
#		duration = -1
#	}
#}
450.1.1 = {
	set_province_flag = has_estuary
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = great_natural_place
	add_permanent_province_modifier = {
		name = elbe_estuary_modifier
		duration = -1
	}
}
1510.1.1 = {
	fort_14th = yes
}
1520.5.5 = {
	base_tax = 4
	base_production = 2
	base_manpower = 0
}
1522.3.20 = {
	naval_arsenal = yes
}
1529.1.1 = {
	religion = protestant
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
#1550.1.1 = {
#	base_tax = 11
#	base_production = 11
#} #gradual shift in trading power from L�beck to Hamburg
#1600.1.1 = {
#	base_tax = 12
#	base_production = 12
#	fort_14th = no
#	fort_17th = yes
#} #gradual shift in trading power from L�beck to Hamburg
1678.1.1 = {
	opera = yes
} #First German Opera
1700.1.1 = {
	fort_17th = no
	fort_18th = yes
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1810.12.13 = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Annexed by France
1814.4.11 = {
	owner = HAM
	controller = HAM
	remove_core = FRA
} # Napoleon abdicates unconditionally
