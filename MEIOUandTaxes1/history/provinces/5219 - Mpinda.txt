# 5219 Mpinda

capital = "Mpinda"
trade_goods = fish
culture = kongolese
religion = animism

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 50
native_ferocity = 1
native_hostileness = 3

discovered_by = sub_saharan
discovered_by = central_african

450.1.1 = {
	set_province_flag = tribals_control_province
	add_permanent_province_modifier = {
		name = "pearls_medium"
		duration = -1
	}
	set_province_flag = has_estuary
	add_permanent_province_modifier = {
		name = "loango_natural_harbour"
		duration = -1
	}
}
1356.1.1 = {
	add_permanent_province_modifier = {
		name = trading_post_province
		duration = -1
	}
}
1481.1.1 = {
	discovered_by = POR
} # Bartolomeu Dias
