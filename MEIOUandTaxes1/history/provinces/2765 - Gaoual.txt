# 2765 - Gaoual

owner = FLO
controller = FLO
add_core = FLO

capital = "Gaoual"
trade_goods = livestock
culture = fulani
religion = sunni

hre = no

base_tax = 4
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 95 }
}
1520.1.1 = {
	base_tax = 12
}
