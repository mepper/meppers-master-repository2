#153 - Pecs

owner = HUN
controller = HUN
add_core = HUN

capital = "P�cs"
trade_goods = livestock #coal
culture = hungarian
religion = catholic

hre = no

base_tax = 11
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes
urban_infrastructure_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
1367.1.1 = {
	small_university = yes
}
1506.1.1 = {
	controller = REB
} # Szekely rebellion
1507.1.1 = {
	controller = HUN
} # Estimated
1514.4.1 = {
	controller = REB
} # Peasant rebellion against Hungary's magnates
1515.1.1 = {
	controller = HUN
} # Estimated
1520.5.5 = {
	base_tax = 12
	base_production = 0
	base_manpower = 1
}
1526.8.30 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	add_permanent_claim = HAB
}
1527.6.1 = {
	fort_14th = yes
} # P�cs is rebuilt and fortified
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1541.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = HAB
} # Without Ferdinand's support, the citizens couldn't hold P�cs anymore
1686.10.22 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = TUR
} # The Ottomans surrender to the army led by Louis of Baden
