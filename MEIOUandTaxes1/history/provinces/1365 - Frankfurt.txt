#1365 - Frankfurt

owner = FRF
controller = FRF
add_core = FRF

capital = "Frankfurt"
trade_goods = wheat # linen
culture = hessian
religion = catholic

hre = yes

base_tax = 3
base_production = 1
base_manpower = 0

is_city = yes
local_fortification_1 = yes
workshop = yes
marketplace = yes
town_hall = yes
road_network = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
	set_province_flag = pocket_province
}
1510.1.1 = {
	fort_14th = yes
}
1530.1.3 = {
	base_tax = 2
	base_production = 2
	road_network = no
	paved_road_network = yes
}
1556.1.1 = {
	religion = protestant
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1807.7.9 = {
	owner = WES
	controller = WES
	add_core = WES
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1813.10.14 = {
	owner = HES
	controller = HES
	add_core = HES
	remove_core = WES
} # Westfalia is dissolved after the Battle of Leipsig
