#74 - Baden

owner = BAD
controller = BAD
add_core = BAD

capital = "Baden"
trade_goods = lumber
culture = rhine_alemanisch
religion = catholic

hre = yes

base_tax = 9
base_production = 1
base_manpower = 1

is_city = yes
town_hall = yes
local_fortification_1 = yes
road_network = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

450.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = iron
	set_province_flag = freeholders_control_province
}
1520.5.5 = {
	base_tax = 11
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1538.1.1 = {
	religion = protestant
} # Protestant majority
1650.1.1 = {
	religion = catholic
}
1706.1.1 = {
	capital = "Rastatt"
}
1792.10.3 = {
	controller = FRA
} # Occupied by French troops
1796.8.7 = {
	controller = BAD
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
