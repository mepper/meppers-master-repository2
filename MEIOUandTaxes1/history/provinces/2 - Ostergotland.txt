# �sterg�tland
# MEIOU - Gigau

owner = SWE
controller = SWE
add_core = SWE

capital = "Link�ping"
trade_goods = lumber
culture = swedish
religion = catholic

hre = no

base_tax = 5
base_production = 0
base_manpower = 0

is_city = yes
temple = yes
local_fortification_1 = yes
#shipyard = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim

1356.1.1 = {
	owner = RSW
	controller = RSW
	add_core = RSW
}
1360.1.1 = {
	owner = SWE
	controller = SWE
	remove_core = RSW
}
1500.3.3 = {
	base_tax = 6
	base_production = 0
	base_manpower = 0
}
1522.2.15 = {
	shipyard = yes
}
1527.6.1 = {
	religion = protestant
}
1529.12.17 = {
	merchant_guild = yes
}
1542.1.1 = {
	fort_14th = yes
}
1625.1.1 = {
	fort_14th = no
	fort_15th = yes
}
#minor court belonging to Svea Hovr�tt
#�stg�ta infanteriregemente
#major upswing in the grain belt
1654.1.1 = { } #Result of "The Great Reduction"
1690.1.1 = {
	capital = "Norrk�ping"
}
1700.1.1 = {
	fort_15th = no
	fort_16th = yes
}
#Due to the support of manufactories
1790.1.1 = {
	fort_16th = no
	fort_17th = yes
}
