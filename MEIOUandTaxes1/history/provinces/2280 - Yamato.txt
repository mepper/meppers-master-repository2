# 2280 - Yamato
# GG/LS - Japanese Civil War

owner = JAP
controller = JAP
add_core = JAP

capital = "Nara"
trade_goods = tea
culture = kansai
religion = mahayana #shinbutsu

hre = no

base_tax = 7
base_production = 1
base_manpower = 0

is_city = yes
road_network = yes
workshop = yes
temple = yes
small_university = yes #YAMATO SCHOOL
town_hall = yes

discovered_by = chinese

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_steel
		duration = -1
	}
}
1356.1.1 = {
	add_core = AKG
}
1392.1.1 = {
	owner = AKG
	controller = AKG
}
1501.1.1 = {
	base_tax = 11
	base_production = 2
	base_manpower = 1
}
1542.1.1 = {
	discovered_by = POR
}
1572.1.1 = {
	owner = ODA
	controller = ODA
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
