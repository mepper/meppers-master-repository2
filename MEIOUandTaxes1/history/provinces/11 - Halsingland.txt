# 11 - H�lsingland
# MEIOU - Gigau

owner = SWE
controller = SWE
add_core = SWE

capital = "G�vle"
trade_goods = lumber
culture = swedish
religion = catholic

hre = no

base_tax = 4
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = eastern
discovered_by = western

1200.1.1 = {
	set_province_flag = freeholders_control_province
}
1500.3.3 = {
	base_tax = 5
	base_production = 0
	base_manpower = 0
}
1522.2.15 = {
	shipyard = yes
}
1527.6.1 = {
	religion = protestant
}
1529.12.17 = {
	merchant_guild = yes
}
#V�sterbottens regemente
1623.1.1 = {
	fort_14th = yes
}
1650.1.1 = {
	trade_goods = lumber
} #Estimated Date
1704.1.1 = {
	fort_14th = yes
}
1809.3.24 = {
	controller = RUS
} # Conquered by Barclay de Tolly
1809.9.17 = {
	controller = SWE
} # Treaty of Fredrikshamn
