# 204 Dauphin� - Principal cities: Grenoble

owner = FRA
controller = FRA

capital = "Greynovol"
trade_goods = iron #lumber
culture = arpitan
religion = catholic

hre = no

base_tax = 20
base_production = 1
base_manpower = 1

is_city = yes
temple = yes
road_network = yes
small_university = yes
local_fortification_1 = yes
#university = yes # L'Universit� de Grenoble

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1349.1.1 = {
	add_core = FRA
	add_core = SAV
} # Dauphin� is sold to Philippe VI
1422.10.21 = {
	owner = DAU
	controller = DAU
	add_core = DAU
	remove_core = FRA
}
1429.7.17 = {
	owner = FRA
	controller = FRA
	add_core = FRA
	remove_core = DAU
	remove_core = SAV
	fort_14th = yes
}
#1461.7.22 = {
#	owner = FRA
#	controller = FRA
#} # Louis is proclaimed king of France
1520.5.5 = {
	base_tax = 24
	base_production = 2
	base_manpower = 1
}
1535.1.1 = {
	fort_14th = yes
}
1560.1.1 = {
	religion = reformed
}
1565.1.1 = {
	unrest = 8
} # France is restless once again as ultra-catholic intentions become clear
1568.9.1 = {
	unrest = 15
} # Catherine de Medici and Charles IX side with the Guise faction, religious intolerance peaks
1570.8.8 = {
	unrest = 10
} # Edict of Saint-Germain: temporary pacification
1573.9.1 = {
	unrest = 15
} # Saint Barthelew's Day Massacre: the consequences in the land
1574.5.1 = {
	unrest = 7
} # Charles IX dies, situation cools a bit
1584.1.1 = {
	unrest = 12
} # Situation heats up again
1588.12.1 = {
	unrest = 15
} # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1594.1.1 = {
	unrest = 10
} # 'Paris vaut bien une messe!', Henri converts to Catholicism
1598.4.13 = {
	unrest = 3
} # Edict of Nantes, a lot more freedom to the protestants
1598.5.2 = {
	unrest = 0
} # Peace of Vervins, formal end to the Wars of Religion
1630.1.1 = {
	fort_14th = no
	fort_15th = yes
}
1650.1.14 = {
	unrest = 7
} # Mazarin arrests the Princes Cond�, Conti & Longueville, the beginning of the Second Fronde
1651.4.1 = {
	unrest = 4
} # An unstable peace is concluded
1651.12.1 = {
	unrest = 7
} # Mazarin returns from exile, Cond� sides with Spain, situation heats up again
1652.10.21 = {
	unrest = 0
} # The King is allowed to enter Paris again, Mazarin leaves France for good. Second Fronde over.
1660.1.1 = {
	fort_15th = no
	fort_16th = yes
}
1680.1.1 = {
	fort_16th = no
	fort_17th = yes
} # Vauban's 'pointed' fort in Brian�on
1685.10.18 = {
	unrest = 8
} # Edict of Nantes revoked by Louis XIV
1686.1.17 = {
	religion = catholic
	unrest = 0
} # Dragonnard campaign successful: region reverts back to catholicism
