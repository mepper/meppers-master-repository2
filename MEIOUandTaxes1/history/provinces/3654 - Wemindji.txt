# 3654 - Speyer

owner = FRS
controller = FRS
add_core = FRS

capital = "Speyer"
trade_goods = indigo
culture = hessian
religion = catholic

hre = yes

base_tax = 2
base_production = 2
base_manpower = 0

is_city = yes
local_fortification_1 = yes
road_network = yes
workshop = yes
marketplace = yes
urban_infrastructure_1 = yes
temple = yes
# The Kaiserdom in Speyer is on of the oldest and greatest in the HRE, built over the 11th and 12th century
# The Reichskammergericht (1495-1806) is the highest court in the HRE situated in Worms and after 1527 Speyer

discovered_by = eastern
discovered_by = western
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
}
1500.1.1 = {
	fort_14th = yes
}
1520.5.5 = {
	base_tax = 3
	base_production = 1
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
