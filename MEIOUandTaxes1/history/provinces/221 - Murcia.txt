# 221 - Murcia + Lorca + Mula

owner = CAS #Juan II of Castille
controller = CAS
add_core = CAS

capital = "Murcia"
trade_goods = fish # silk
culture = andalucian # culture = murcian
religion = catholic

hre = no

base_tax = 8
base_production = 2
base_manpower = 1

is_city = yes
workshop = yes
urban_infrastructure_1 = yes
harbour_infrastructure_2 = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_silk
		duration = -1
	}
}
1356.1.1 = {
	set_province_flag = spanish_name
	add_claim = ARA
	owner = ENR
	controller = ENR
	add_core = ENR
	add_permanent_province_modifier = {
		name = "lordship_of_murcia"
		duration = -1
	}
}
1369.3.23 = {
	remove_core = ENR
	owner = CAS
	controller = CAS
}
1400.1.1 = {
	fort_14th = yes
}
1467.10.1 = {
	temple = yes
}
1470.1.1 = {
	trade_goods = alum
}
1500.3.3 = {
	base_tax = 8
	base_production = 3
	base_manpower = 1
}
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	road_network = no
	paved_road_network = yes
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castille
1713.4.11 = {
	remove_core = CAS
}
1808.6.6 = {
	controller = REB
}
1811.1.1 = {
	controller = SPA
}
1812.10.1 = {
	controller = REB
}
1813.12.11 = {
	controller = SPA
}
