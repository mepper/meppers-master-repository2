# 2621 - Prignitz

owner = BRA
controller = BRA
add_core = BRA

capital = "Perleberg"
trade_goods = livestock
culture = low_saxon
religion = catholic

hre = yes

base_tax = 5
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = {
	road_network = yes
}
1515.1.1 = {
	fort_14th = yes
}
1525.1.1 = {
	fort_14th = yes
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1539.1.1 = {
	religion = protestant
}
1650.1.1 = {
	culture = prussian
}
1675.1.1 = {
	fort_14th = no
	fort_15th = yes
}
#Immigration of Huegenots boosts economy
1701.1.18 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = BRA
} # Friedrich III becomes king in Prussia
1725.1.1 = {
	fort_15th = no
	fort_16th = yes
}
1775.1.1 = {
	fort_16th = no
	fort_17th = yes
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1806.10.27 = {
	controller = FRA
}
1807.7.9 = {
	controller = PRU
} # The Second treaty of Tilsit
