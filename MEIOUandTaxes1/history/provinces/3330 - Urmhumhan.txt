# No previous file for Eastern Munster

owner = BTU
controller = BTU
add_core = BTU

capital = "Carraig na Si�ire" # Carrick-on-Suir
trade_goods = fish
culture = irish
religion = catholic

hre = no

base_tax = 4
base_production = 0
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes
temple = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

700.1.1 = {
	add_permanent_province_modifier = {
		name = clan_land
		duration = -1
	}
}
1520.5.5 = {
	base_tax = 6
	base_production = 0
	base_manpower = 0
}
1529.1.1 = {
	owner = ENG
	controller = ENG
	add_core = ENG
} # The Butlers were loyal to Henry VIII and from here on served England
1642.1.1 = {
	controller = REB
} # Estimated
1642.6.7 = {
	owner = IRE
	controller = IRE
} # Confederation of Kilkenny
1650.4.10 = {
	controller = ENG
} # Battle of Macroom
1652.4.1 = {
	owner = ENG
} # End of the Irish Confederates
1655.1.1 = {
	fort_14th = yes
} # Estimated
1689.3.12 = {
	controller = REB
} # James II Lands in Ireland
1690.9.15 = {
	controller = ENG
}
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
