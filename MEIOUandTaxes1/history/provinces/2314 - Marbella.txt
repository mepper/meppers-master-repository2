
owner = GRA # Mustapha Sa'd King of Granada
controller = GRA
add_core = GRA

capital = "Marballa"
trade_goods = sugar
culture = andalucian
religion = sunni

hre = no

base_tax = 9
base_production = 0
base_manpower = 0

is_city = yes
fort_14th = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech

1356.1.1 = {
	set_province_flag = granada_emirate
	set_province_flag = arabic_name
}
1462.1.1 = {
	owner = CAS
	controller = CAS
	add_core = CAS
	trade_goods = wine
	change_province_name = "Marbella"
	rename_capital = "Marbella"
	remove_core = GRA
} # Conquest of Gibraltar by King Enrique of Castilla
1499.12.1 = {
	unrest = 2
} # The Inquisition forces Spanish muslims to convert back to Christianity. Occasional revolts occur.
1500.3.3 = {
	base_tax = 9
	base_production = 1
	base_manpower = 0
}
1502.2.1 = {
	unrest = 0
	religion = catholic
} # New capitulations where all the subjects of Granada are baptised and fully incorporated into the legal system of Castilla
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = CAS
	road_network = no
	paved_road_network = yes
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
1610.1.12 = { } # Decree for the expulsion of the morisques in Andaluc�a, which is speedily and uneventfully performed
