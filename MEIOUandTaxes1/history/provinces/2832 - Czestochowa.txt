# 2832 - Czestochowa

owner = POL
controller = POL
add_core = POL

capital = "Czestochowa"
trade_goods = wheat
culture = polish
religion = catholic

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes
road_network = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1355.1.1 = {
	add_permanent_province_modifier = {
		name = polish_estates
		duration = -1
	}
	set_province_flag = mined_goods
	set_province_flag = lead
}
1520.5.5 = {
	base_tax = 5
}
1569.7.1 = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1588.1.1 = {
	controller = REB
} # Civil war
1589.1.1 = {
	controller = PLC
} # Coronation of Sigismund III
1606.1.1 = {
	controller = REB
} # Civil war
1608.1.1 = {
	controller = PLC
} # Minor victory of Sigismund
1655.1.1 = {
	controller = SWE
} # The Deluge
1657.1.1 = {
	unrest = 0
} # Rebellion fails
1660.1.1 = {
	controller = PLC
}
1733.1.1 = {
	controller = REB
} # The war of Polish succession
1735.1.1 = {
	controller = PLC
}
1793.1.23 = {
	controller = PRU
	owner = PRU
	add_core = PRU
	add_core = POL
	remove_core = PLC
} # Second partition
1807.7.9 = {
	owner = POL
	controller = POL
} # The Duchy of Warsaw is established after the treaty of Tilsit, ruled by Frederick Augustus I of Saxony
1812.12.12 = {
	controller = PRU
}
1814.4.11 = {
	controller = POL
}
1815.6.9 = {
	add_core = RUS
} # Congress Poland, under Russian control after the Congress of Vienna