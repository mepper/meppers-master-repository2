# No previous file for Bacau

owner = MOL
controller = MOL
add_core = MOL

capital = "Bacau"
trade_goods = wine
culture = moldovian
religion = orthodox

hre = no

base_tax = 7
base_production = 0
base_manpower = 0

is_city = yes
temple = yes
#fort_14th = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1000.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = salt
}
1356.1.1 = {
	add_permanent_claim = MOL
}
#1356.1.1 = {
#	owner = HUN
#	controller = HUN
#	add_core = HUN
#}
#1396.1.1 = {
#	owner = MOL
#	controller = MOL
#	add_core = MOL
#}
#1498.1.1 = {
#	add_core = TUR
#} # Bayezid II forces Stephen the Great to accept Ottoman suzereignty.
1520.5.5 = {
	base_tax = 10
	base_production = 0
	base_manpower = 1
}
1812.5.28 = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = TUR
} # Treaty of Bucarest ending the Russo-Turkidh War
