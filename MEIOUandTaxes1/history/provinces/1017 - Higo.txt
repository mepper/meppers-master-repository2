# 1017 - Higo
# GG/LS - Japanese Civil War

owner = KKC
controller = KKC
add_core = KKC

capital = "Kumamoto"
trade_goods = lumber
culture = kyushu
religion = mahayana #shinbutsu

hre = no

base_tax = 29
base_production = 1
base_manpower = 2

is_city = yes
town_hall = yes
harbour_infrastructure_1 = yes
local_fortification_1 = yes

discovered_by = chinese

1501.1.1 = {
	base_tax = 52
}
1542.1.1 = {
	discovered_by = POR
}
1585.1.1 = {
	religion = catholic
}
1637.1.1 = {
	religion = mahayana #shinbutsu
}
1650.1.1 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
