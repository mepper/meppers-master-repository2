# 421 - Lakshadwwep

capital = "Lakshadweep"
trade_goods = unknown
culture = malayalam
religion = sunni

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 5
native_ferocity = 3
native_hostileness = 1

discovered_by = indian

1498.5.15 = {
	discovered_by = POR
} # FB was 1498.1.1
1787.1.1 = {
	owner = MYS
	controller = MYS
	trade_goods = fish
	base_tax = 1
	set_province_flag = trade_good_set
}
1789.1.1 = {
	owner = GBR
	controller = GBR
} # The British took over the administration of the islands for non-payment of arrears.
