# 3352 Navia

owner = CAS #Juan II of Castille
controller = CAS
add_core = CAS

capital = "Tineo"
trade_goods = lumber
culture = asturian
religion = catholic

hre = no

base_tax = 8
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

450.1.1 = {
	add_permanent_province_modifier = {
		name = "lordship_of_asturias"
		duration = -1
	}
}
1356.1.1 = {
	owner = ENR
	controller = ENR
	add_core = ENR
	add_core = LEO
}
1369.3.23 = {
	remove_core = ENR
	owner = CAS
	controller = CAS
}
1400.1.1 = {
	fort_14th = yes
}
1475.6.2 = {
	controller = POR
}
1476.3.2 = {
	controller = CAS
}
1500.3.3 = {
	base_tax = 8
	base_production = 1
	base_manpower = 0
}
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = CAS
	remove_core = LEO
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castilla
1713.4.11 = {
	remove_core = LEO
}
# Audiencia de Asturias
1808.5.6 = {
	controller = REB
}
1809.1.1 = {
	controller = SPA
}
1812.10.1 = {
	controller = REB
}
1813.12.11 = {
	controller = SPA
}
