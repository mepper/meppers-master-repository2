# 3729 - Celle

owner = LUN
controller = LUN
add_core = LUN

capital = "Celle"
trade_goods = livestock
culture = eastphalian
religion = catholic

hre = yes

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = {
	road_network = yes
}
1529.1.1 = {
	religion = protestant
}
1705.1.1 = {
	owner = HAN
	controller = HAN
	add_core = HAN
	remove_core = LUN
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1807.7.9 = {
	owner = WES
	controller = WES
	add_core = WES
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1813.10.14 = {
	owner = HAN
	controller = HAN
	add_core = HAN
	remove_core = WES
} # Congress of Vienna, Luneburg is incorporated into the Kingdom of Hannover
