# 1026 - Aki
# GG/LS - Japanese Civil War

owner = OUC
controller = OUC
add_core = OUC

capital = "Firoshima"
trade_goods = rice
culture = chugoku
religion = mahayana #shinbutsu

hre = no

base_tax = 20
base_production = 1
base_manpower = 2

is_city = yes
town_hall = yes
harbour_infrastructure_1 = yes
temple = yes

discovered_by = chinese

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "hiroshima_natural_harbour"
		duration = -1
	}
}
1356.1.1 = {
	add_core = MRI
}
1501.1.1 = {
	base_tax = 32
	base_production = 3
	base_manpower = 4
}
1542.1.1 = {
	discovered_by = POR
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
