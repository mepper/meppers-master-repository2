# 60 - Oberlausitz

owner = BOH
controller = BOH
add_core = BOH

capital = "Budysin"
trade_goods = carmine
culture = sorbs
religion = catholic

hre = yes

base_tax = 8
base_production = 1
base_manpower = 1

is_city = yes
local_fortification_1 = yes
town_hall = yes
workshop = yes
marketplace = yes
temple = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1319.1.1 = {
	add_permanent_province_modifier = {
		name = bohemian_estates
		duration = -1
	}
}
1356.1.1 = {
	add_core = BRA
}
1444.1.1 = {
	add_core = BRA
}
1457.1.1 = {
	unrest = 5
} # George of Podiebrand had to secure recognition from the German and Catholic towns. Pilsen is very hostile towards him, the Roman church being dominant throughout Pilsen's history.
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 9
	base_production = 2
	base_manpower = 1
}
1526.8.30 = {
	owner = HAB
	controller = HAB
	add_core = HAB
} #Battle of Mohac where Lajos II dies -> Habsburg succession
1530.1.2 = {
	religion = protestant
}
1537.1.1 = {
	fort_14th = yes
}
1618.4.23 = {
	revolt = {
		type = protestant_rebels
		size = 2
	}
	controller = REB
} # Defenstration of Prague
1619.3.1 = {
	revolt = { }
	controller = PAL
	owner = PAL
	add_core = PAL
} #Fredrik of PAL accepts to become King of BOH.
1620.11.8 = {
	controller = SAX
	owner = BOH
	remove_core = PAL
}# Tilly beats the Winterking, but HAB gives the province to Saxony as security against a loan.
1635.5.1 = {
	owner = SAX
	add_core = SAX
	remove_core = HAB
}
1701.1.18 = {
	add_core = PRU
	remove_core = BRA
} # Friedrich III becomes king in Prussia
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1815.6.9 = {
	owner = PRU
	controller = PRU
	remove_core = SAX
} # Congress of Vienna
