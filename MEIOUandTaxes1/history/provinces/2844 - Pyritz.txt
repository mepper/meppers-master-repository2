# 2844 - Vorpommern-Pyritz

owner = POM
controller = POM
add_core = POM

capital = "Pyritz"
trade_goods = lumber
culture = pommeranian
religion = catholic

hre = yes

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes
road_network = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1295.1.1 = {
	owner = PST
	controller = PST
	add_core = PST
	remove_core = POM
}
1478.1.1 = {
	owner = POM
	controller = POM
	add_core = POM
	remove_core = PST
} # Duchy reunited for a short period
#1529.1.1 = {
#	add_core = BRA
#}
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 4
}
1531.1.1 = {
	owner = PST
	controller = PST
	add_core = PST
	remove_core = POM
} # Fifth Partition
1534.1.1 = {
	religion = protestant
	fort_14th = yes
}
1625.1.1 = {
	owner = POM
	controller = POM
	add_core = POM
	remove_core = PST
} # Final reunification
1630.7.10 = {
	owner = SWE
	controller = SWE
	add_core = SWE
} # Treaty of Stettin
1648.10.24 = {
	owner = BRA
	controller = BRA
	add_core = BRA
	remove_core = SWE
} # Treaty of Westphalia
1701.1.18 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = BRA
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
