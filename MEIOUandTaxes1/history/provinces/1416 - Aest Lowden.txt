# 1416 - Aest Lowden

owner = SCO
controller = SCO
add_core = SCO

capital = "Dunbar"
trade_goods = wool
culture = lowland_scottish
religion = catholic

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes
fort_14th = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

1250.1.1 = {
	add_permanent_province_modifier = {
		name = marches_of_the_south
		duration = -1
	}
}
1482.8.1 = {
	controller = ENG
}
1483.3.1 = {
	controller = SCO
}
1520.5.5 = {
	base_tax = 3
	base_production = 0
	base_manpower = 0
}
1547.10.1 = {
	controller = ENG
} #Rough Wooing
1550.1.1 = {
	controller = SCO
} #Scots Evict English with French Aid
1560.1.1 = {
	fort_14th = yes
}
1560.8.1 = {
	religion = reformed
}
1603.3.24 = {
	remove_province_modifier = "marches_of_the_south"
}
1650.12.24 = {
	controller = ENG
} #Cromwell Captures Edinburgh Castle
1652.4.21 = {
	controller = SCO
} #Union of Scotland and the Commonwealth
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
1750.1.1 = { } #Regimental Camp, Tax Assessor Estimated
