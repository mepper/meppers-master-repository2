# GAL - Galicia

government = feudal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = galician
religion = catholic
technology_group = western
capital = 206	# Galicia

historical_rival = CAS
historical_rival = LEO
historical_rival = SPA

1000.1.1 = {
	add_country_modifier = { name = title_2 duration = -1 }
	set_country_flag = title_2
	#set_variable = { which = "centralization_decentralization" value = -3 }
	add_absolutism = -100
	add_absolutism = 80
}
