# KRL - Karelia
# 2010-jan-21 - FB - HT3 changes

government = feudal_monarchy government_rank = 1
mercantilism = 0.0
# serfdom_freesubjects = 4
technology_group = eastern
religion = catholic
primary_culture = finnish
capital = 27	# �bo

1000.1.1 = {
	add_country_modifier = { name = title_4 duration = -1 }
	set_country_flag = title_4
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 20
}
