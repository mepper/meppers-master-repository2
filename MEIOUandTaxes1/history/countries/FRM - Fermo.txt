# FRM - Fermo

government = oligarchic_republic government_rank = 1
mercantilism = 0.0
primary_culture = umbrian
religion = catholic
technology_group = western
capital = 3702

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	add_absolutism = -100
	add_absolutism = 20
}

1355.1.1 = {
	monarch = {
		name = "Republican Council"
		ADM = 4
		DIP = 4
		MIL = 2
	}
}
