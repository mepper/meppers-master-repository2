#TUA - Kel Ahaggar

government = tribal_nomads government_rank = 1
mercantilism = 0.0
primary_culture = tuareg
religion = sunni
technology_group = soudantech
capital = 1543 # Hoggar

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 0
}

1340.1.1 = {
	monarch = { #Fictional
		name = "Umaru"
		dynasty = "Kel Ahaggar"
		dip = 3
		adm = 3
		mil = 3
	}
}