# OGA - Ogasawara
# LS/GG - Japanese Civil War
# 2010-jan-20 - FB - HT3
# 2013-aug-07 - GG - EUIV changes

government = japanese_monarchy
government_rank = 1
# aristocracy_plutocracy = -4
# centralization_decentralization = 2
# innovative_narrowminded = 2
mercantilism = 0.0 # mercantilism_freetrade = -5
# offensive_defensive = -1
# land_naval = 0
# quality_quantity = 4
# serfdom_freesubjects = -5
# isolationist_expansionist = -3
# secularism_theocracy = 0
primary_culture = kanto
religion = mahayana
technology_group = chinese
capital = 2290	# Shinano

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}

1319.7.28 = {
	monarch = {
		name = "Masanaga"
		dynasty = "Ogasawara"
		ADM = 4
		DIP = 2
		MIL = 4
	}
}

1347.3.9 = {
	heir = {
		name = "Nagamoto"
		monarch_name = "Nagamoto"
		dynasty = "Ogasawara"
		birth_date = 1347.3.9
		death_date = 1407.11.5
		claim = 90
		ADM = 3
		DIP = 4
		MIL = 3
	}
}
