# ZIE - Ziegenhain

government = feudal_monarchy
government_rank = 1
mercantilism = 0.0
primary_culture = hessian
religion = catholic
technology_group = western
capital = 3731

1000.1.1 = {
	add_country_modifier = { name = title_1 duration = -1 }
	set_country_flag = title_1
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}

1349.1.1   = {
	monarch = {
		name = "Gottfried VII"
		dynasty = "von Ziegenheim"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1372.1.1   = {
	monarch = {
		name = "Gottfried VIII"
		dynasty = "von Ziegenheim"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1394.1.1   = {
	monarch = {
		name = "Johann II"
		dynasty = "von Ziegenheim"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

# 1450.2.14 - inherited by Hesse
