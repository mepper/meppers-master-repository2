# SHU - Shu
# LS - Chinese Civil War

government = chinese_monarchy_5 government_rank = 1
mercantilism = 0.0
technology_group = chinese
religion = mahayana
primary_culture = bashu
capital = 1337

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = -2 }
	add_absolutism = -100
	add_absolutism = 70
}

1356.1.1 = {
	monarch = {	#Li Xixi
		name = "Xixi"
		dynasty = "Li"
		ADM = 2
		DIP = 4
		MIL = 4
	}
	set_country_flag = red_turban_reb
	leader = {
		name = "Buxin Bai"
		type = general
		fire = 2
		shock = 2
		manuever = 3
		siege = 1
		death_date = 1360.1.1
	}
}