# CRT - Crete

government = feudal_monarchy government_rank = 1 #KINGDOM
mercantilism = 0.0
technology_group = eastern
primary_culture = greek
#add_accepted_culture = greek
religion = orthodox
capital = 163	# Heraklion

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = 0 }
	add_absolutism = -100
	add_absolutism = 30
}
