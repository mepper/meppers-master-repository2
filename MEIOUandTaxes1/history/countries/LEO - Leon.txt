# LEO - Kingdom  of Le�n
# 2010-jan-21 - FB - HT3 changes

government = feudal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = asturian
add_accepted_culture = galician
religion = catholic
technology_group = western
capital = 208

historical_rival = CAS
historical_rival = GAL
historical_rival = SPA

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = -3 }
	add_absolutism = -100
	add_absolutism = 80
}
