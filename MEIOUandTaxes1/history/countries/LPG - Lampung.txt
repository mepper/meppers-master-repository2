# LPG - Lampung
# MEIOU-FB Indonesia mod
# FB-TODO needs rulers until 1552.1.1
# 2010-jan-21 - FB - HT3 changes

government = eastern_monarchy government_rank = 1
mercantilism = 0.0
technology_group = austranesian		# MEIOU-FB - was: chinese
primary_culture = sumatran
religion = hinduism
capital = 623

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = 0 }
	add_absolutism = -100
	add_absolutism = 30
}

1337.1.1 = {
	monarch = {
		name = "Purwawisesa"
		dynasty = "Gunungjati"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
