# Bohemian Missions

bohemia_silesia_relations = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = BOH
		exists = SIL
		is_free_or_tributary_trigger = yes
		government = monarchy
		NOT = { war_with = SIL }
		NOT = { marriage_with = SIL }
		SIL = { government = monarchy }
		NOT = { FROM = { is_rival = ROOT } }
		NOT = { ROOT = { is_rival = FROM } }
		NOT = { has_country_modifier = foreign_contacts }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { government = monarchy }
			NOT = { exists = SIL }
			war_with = SIL
			SIL = {
				NOT = { government = monarchy }
			}
		}
	}
	success = {
		marriage_with = SIL
		SIL = { has_opinion = { who = BOH value = 100 } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = SIL value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = SIL value = -100 } }
		}
	}
	effect = {
		add_dip_power = 30
		add_country_modifier = {
			name = "foreign_contacts"
			duration = 365
		}
	}
}


diplo_annex_silesia = {
	
	type = country
	
	category = DIP
	
	target_provinces = {
		owned_by = SIL
	}
	allow = {
		tag = BOH
		exists = SIL
		is_free_or_tributary_trigger = yes
		dip = 4
		SIL = {
			vassal_of = BOH
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			religion_group = ROOT
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = SIL }
			SIL = {
				NOT = { religion_group = ROOT }
			}
		}
	}
	success = {
		NOT = { exists = SIL }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_opinion = { who = SIL value = 100 }
		}
		modifier = {
			factor = 2
			has_opinion = { who = SIL value = 200 }
		}
	}
	effect = {
		add_prestige = 10
		add_adm_power = 50
		add_dip_power = 50
	}
}


bohemia_hungary_relations = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = BOH
		exists = HUN
		NOT = { war_with = HUN }
		NOT = { has_opinion = { who = HUN value = 100 } }
		NOT = { FROM = { is_rival = ROOT } }
		NOT = { ROOT = { is_rival = FROM } }
		NOT = { has_country_modifier = foreign_contacts }
	}
	abort = {
		OR = {
			NOT = { exists = HUN }
			war_with = HUN
		}
	}
	success = {
		HUN = { has_opinion = { who = BOH value = 150 } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = HUN value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = HUN value = -100 } }
		}
	}
	effect = {
		add_dip_power = 30
		add_country_modifier = {
			name = "foreign_contacts"
			duration = 365
		}
	}
}


defend_bohemia_against_hungary = {
	
	type = country
	
	category = MIL
	
	allow = {
		tag = BOH
		exists = HUN
		is_free_or_tributary_trigger = yes
		bohemia_region = {
			owned_by = HUN
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = HUN }
		}
	}
	success = {
		NOT = { war_with = HUN }
		NOT = { bohemia_region = { owned_by = HUN } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			HUN = { NOT = { mil = 2 } }
		}
	}
	effect = {
		add_army_tradition = 25
		add_stability_1 = yes
		add_country_modifier = {
			name = "fought_back_the_hungarians"
			duration = 3650
		}
	}
}


bohemia_austria_relations = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = BOH
		exists = HAB
		is_free_or_tributary_trigger = yes
		government = monarchy
		NOT = { war_with = HAB }
		NOT = { marriage_with = HAB }
		NOT = { has_country_modifier = foreign_contacts }
		NOT = { has_opinion = { who = HAB value = 100 } }
		HAB = {
			is_free_or_tributary_trigger = yes
			government = monarchy
		}
		NOT = { FROM = { is_rival = ROOT } }
		NOT = { ROOT = { is_rival = FROM } }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { government = monarchy }
			war_with = HAB
			NOT = { exists = HAB }
			HAB = {
				OR = {
					is_subject_other_than_tributary_trigger = yes
					NOT = { government = monarchy }
				}
			}
		}
	}
	success = {
		marriage_with = HAB
		HAB = { has_opinion = { who = BOH value = 150 } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = HAB value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = HAB value = -100 } }
		}
	}
	effect = {
		add_dip_power = 30
		add_country_modifier = {
			name = "foreign_contacts"
			duration = 365
		}
	}
}
