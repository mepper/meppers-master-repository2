########################################
# Flavor Events for Syria
#
# Scripted by GiGau
########################################

province_event = {
	id = flavor_syr.1
	title = "flavor_syr.1.name"
	desc = "flavor_syr.1.desc"
	picture = catholic_crusade_successful
	
	trigger = {
		province_id = 1334 # Bayrut
		OR = {
			check_variable = { which = chaldean value = 2 }
			check_variable = { which = druze value = 2 }
		}
		owner = {
			NOT = { religion = druze }
			NOT = { religion = chaldean }
		}
		NOT = { has_province_modifier = mount_lebanon_emirate_loyal }
		NOT = { has_province_modifier = mount_lebanon_emirate_disloyal }
		NOT = { has_province_modifier = mount_lebanon_emirate_rebellious }
	}
	
	mean_time_to_happen = {
		years = 100
		
		modifier = {
			owned_by = TUR
			factor = 0.01
		}
		modifier = {
			NOT = { owner = { stability = 1 } }
			factor = 0.90
		}
		modifier = {
			NOT = { owner = { stability = 0 } }
			factor = 0.90
		}
		modifier = {
			NOT = { owner = { stability = -1 } }
			factor = 0.90
		}
		modifier = {
			NOT = { owner = { stability = -2 } }
			factor = 0.90
		}
		modifier = {
			NOT = { owner = { stability = -3 } }
			factor = 0.90
		}
		modifier = {
			owner = { NOT = { absolutism = 10 } }
			factor = 0.90
		}
		modifier = {
			owner = { NOT = { absolutism = 20 } }
			factor = 0.90
		}
		modifier = {
			owner = { NOT = { absolutism = 30 } }
			factor = 0.90
		}
		modifier = {
			owner = { NOT = { absolutism = 40 } }
			factor = 0.90
		}
		modifier = {
			owner = { NOT = { absolutism = 50 } }
			factor = 0.90
		}
		modifier = {
			owner = { absolutism = 60 }
			factor = 1.10
		}
		modifier = {
			owner = { absolutism = 70 }
			factor = 1.10
		}
		modifier = {
			owner = { absolutism = 80 }
			factor = 1.10
		}
		modifier = {
			owner = { absolutism = 90 }
			factor = 1.10
		}
		modifier = {
			owner = { absolutism = 100 }
			factor = 1.10
		}
	}
	
	option = {
		name = "flavor_syr.1.opta"
		ai_chance = {
			factor = 100
		}
		add_permanent_province_modifier = {
			name = "mount_lebanon_emirate_loyal"
			duration = -1
		}
	}
	option = {
		name = "flavor_syr.1.optb"
		ai_chance = {
			factor = 0
		}
		if = {
			limit = {
				check_variable = { which = chaldean value = 2 }
				NOT = { check_variable = { which = druze value = 2 } }
			}
			spawn_rebels = {
				type = chaldean_rebels
				size = 2
			}
		}
		if = {
			limit = {
				NOT = { check_variable = { which = chaldean value = 2 } }
				check_variable = { which = druze value = 2 }
			}
			spawn_rebels = {
				type = druze_rebels
				size = 2
			}
		}
		if = {
			limit = {
				OR = {
					check_variable = { which = chaldean value = 2 }
					check_variable = { which = druze value = 2 }
				}
			}
			random_list = {
				50 = {
					spawn_rebels = {
						type = chaldean_rebels
						size = 2
					}
				}
				50 = {
					spawn_rebels = {
						type = druze_rebels
						size = 2
					}
				}
			}
		}
	}
}

province_event = {
	id = flavor_syr.2
	title = "flavor_syr.2.name"
	desc = "flavor_syr.2.desc"
	picture = catholic_crusade_successful
	
	trigger = {
		province_id = 2635 # Al Jalil
		OR = {
			check_variable = { which = chaldean value = 2 }
			check_variable = { which = druze value = 2 }
		}
		owner = {
			NOT = { religion = druze }
			NOT = { religion = chaldean }
		}
		NOT = { has_province_modifier = mount_lebanon_emirate_loyal }
		NOT = { has_province_modifier = mount_lebanon_emirate_disloyal }
		NOT = { has_province_modifier = mount_lebanon_emirate_rebellious }
		any_neighbor_province = {
			OR = {
				has_province_modifier = mount_lebanon_emirate_loyal
				has_province_modifier = mount_lebanon_emirate_disloyal
				has_province_modifier = mount_lebanon_emirate_rebellious
			}
		}
	}
	
	mean_time_to_happen = {
		years = 100
		
		modifier = {
			owned_by = TUR
			factor = 0.01
		}
		modifier = {
			NOT = { owner = { stability = 1 } }
			factor = 0.90
		}
		modifier = {
			NOT = { owner = { stability = 0 } }
			factor = 0.90
		}
		modifier = {
			NOT = { owner = { stability = -1 } }
			factor = 0.90
		}
		modifier = {
			NOT = { owner = { stability = -2 } }
			factor = 0.90
		}
		modifier = {
			NOT = { owner = { stability = -3 } }
			factor = 0.90
		}
		modifier = {
			owner = { NOT = { absolutism = 10 } }
			factor = 0.90
		}
		modifier = {
			owner = { NOT = { absolutism = 20 } }
			factor = 0.90
		}
		modifier = {
			owner = { NOT = { absolutism = 30 } }
			factor = 0.90
		}
		modifier = {
			owner = { NOT = { absolutism = 40 } }
			factor = 0.90
		}
		modifier = {
			owner = { NOT = { absolutism = 50 } }
			factor = 0.90
		}
		modifier = {
			owner = { absolutism = 60 }
			factor = 1.10
		}
		modifier = {
			owner = { absolutism = 70 }
			factor = 1.10
		}
		modifier = {
			owner = { absolutism = 80 }
			factor = 1.10
		}
		modifier = {
			owner = { absolutism = 90 }
			factor = 1.10
		}
		modifier = {
			owner = { absolutism = 100 }
			factor = 1.10
		}
		modifier = {
			any_neighbor_province = { has_province_modifier = mount_lebanon_emirate_disloyal }
			factor = 0.75
		}
		modifier = {
			any_neighbor_province = { has_province_modifier = mount_lebanon_emirate_rebellious }
			factor = 0.50
		}
	}
	
	option = {
		name = "flavor_syr.2.opta"
		if = {
			limit = {
				any_neighbor_province = { has_province_modifier = mount_lebanon_emirate_loyal }
			}
			add_permanent_province_modifier = {
				name = "mount_lebanon_emirate_loyal"
				duration = -1
			}
		}
		if = {
			limit = {
				any_neighbor_province = { has_province_modifier = mount_lebanon_emirate_disloyal }
			}
			add_permanent_province_modifier = {
				name = "mount_lebanon_emirate_disloyal"
				duration = -1
			}
		}
		if = {
			limit = {
				any_neighbor_province = { has_province_modifier = mount_lebanon_emirate_rebellious }
			}
			add_permanent_province_modifier = {
				name = "mount_lebanon_emirate_rebellious"
				duration = -1
			}
		}
	}
}

province_event = {
	id = flavor_syr.3
	title = "flavor_syr.3.name"
	desc = "flavor_syr.3.desc"
	picture = catholic_crusade_successful
	
	trigger = {
		province_id = 378 # Tarabulus Al Sham
		OR = {
			check_variable = { which = chaldean value = 2 }
			check_variable = { which = druze value = 2 }
		}
		owner = {
			NOT = { religion = druze }
			NOT = { religion = chaldean }
		}
		NOT = { has_province_modifier = mount_lebanon_emirate_loyal }
		NOT = { has_province_modifier = mount_lebanon_emirate_disloyal }
		NOT = { has_province_modifier = mount_lebanon_emirate_rebellious }
		any_neighbor_province = {
			OR = {
				has_province_modifier = mount_lebanon_emirate_loyal
				has_province_modifier = mount_lebanon_emirate_disloyal
				has_province_modifier = mount_lebanon_emirate_rebellious
			}
		}
	}
	
	mean_time_to_happen = {
		years = 100
		
		modifier = {
			owned_by = TUR
			factor = 0.01
		}
		modifier = {
			NOT = { owner = { stability = 1 } }
			factor = 0.90
		}
		modifier = {
			NOT = { owner = { stability = 0 } }
			factor = 0.90
		}
		modifier = {
			NOT = { owner = { stability = -1 } }
			factor = 0.90
		}
		modifier = {
			NOT = { owner = { stability = -2 } }
			factor = 0.90
		}
		modifier = {
			NOT = { owner = { stability = -3 } }
			factor = 0.90
		}
		modifier = {
			owner = { has_real_disaster_trigger = yes }
			factor = 0.10
		}
		modifier = {
			owner = { NOT = { absolutism = 10 } }
			factor = 0.90
		}
		modifier = {
			owner = { NOT = { absolutism = 20 } }
			factor = 0.90
		}
		modifier = {
			owner = { NOT = { absolutism = 30 } }
			factor = 0.90
		}
		modifier = {
			owner = { NOT = { absolutism = 40 } }
			factor = 0.90
		}
		modifier = {
			owner = { NOT = { absolutism = 50 } }
			factor = 0.90
		}
		modifier = {
			owner = { absolutism = 60 }
			factor = 1.10
		}
		modifier = {
			owner = { absolutism = 70 }
			factor = 1.10
		}
		modifier = {
			owner = { absolutism = 80 }
			factor = 1.10
		}
		modifier = {
			owner = { absolutism = 90 }
			factor = 1.10
		}
		modifier = {
			owner = { absolutism = 100 }
			factor = 1.10
		}
		modifier = {
			any_neighbor_province = { has_province_modifier = mount_lebanon_emirate_disloyal }
			factor = 0.75
		}
		modifier = {
			any_neighbor_province = { has_province_modifier = mount_lebanon_emirate_rebellious }
			factor = 0.50
		}
	}
	
	option = {
		name = "flavor_syr.3.opta"
		if = {
			limit = {
				any_neighbor_province = { has_province_modifier = mount_lebanon_emirate_loyal }
			}
			add_permanent_province_modifier = {
				name = "mount_lebanon_emirate_loyal"
				duration = -1
			}
		}
		if = {
			limit = {
				any_neighbor_province = { has_province_modifier = mount_lebanon_emirate_disloyal }
			}
			add_permanent_province_modifier = {
				name = "mount_lebanon_emirate_disloyal"
				duration = -1
			}
		}
		if = {
			limit = {
				any_neighbor_province = { has_province_modifier = mount_lebanon_emirate_rebellious }
			}
			add_permanent_province_modifier = {
				name = "mount_lebanon_emirate_rebellious"
				duration = -1
			}
		}
	}
}

province_event = {
	id = flavor_syr.4
	title = "flavor_syr.4.name"
	desc = "flavor_syr.4.desc"
	picture = catholic_crusade_successful
	
	trigger = {
		province_id = 378 # Tarabulus Al Sham
		OR = {
			check_variable = { which = chaldean value = 2 }
			check_variable = { which = druze value = 2 }
		}
		owner = {
			NOT = { religion = druze }
			NOT = { religion = chaldean }
		}
		NOT = { has_province_modifier = mount_lebanon_emirate_loyal }
		NOT = { has_province_modifier = mount_lebanon_emirate_disloyal }
		NOT = { has_province_modifier = mount_lebanon_emirate_rebellious }
		any_neighbor_province = {
			OR = {
				has_province_modifier = mount_lebanon_emirate_loyal
				has_province_modifier = mount_lebanon_emirate_disloyal
				has_province_modifier = mount_lebanon_emirate_rebellious
			}
		}
	}
	
	mean_time_to_happen = {
		years = 100
		
		modifier = {
			owned_by = TUR
			factor = 0.01
		}
		modifier = {
			NOT = { owner = { stability = 1 } }
			factor = 0.90
		}
		modifier = {
			NOT = { owner = { stability = 0 } }
			factor = 0.90
		}
		modifier = {
			NOT = { owner = { stability = -1 } }
			factor = 0.90
		}
		modifier = {
			NOT = { owner = { stability = -2 } }
			factor = 0.90
		}
		modifier = {
			NOT = { owner = { stability = -3 } }
			factor = 0.90
		}
		modifier = {
			owner = { has_real_disaster_trigger = yes }
			factor = 0.10
		}
		modifier = {
			owner = { NOT = { absolutism = 10 } }
			factor = 0.90
		}
		modifier = {
			owner = { NOT = { absolutism = 20 } }
			factor = 0.90
		}
		modifier = {
			owner = { NOT = { absolutism = 30 } }
			factor = 0.90
		}
		modifier = {
			owner = { NOT = { absolutism = 40 } }
			factor = 0.90
		}
		modifier = {
			owner = { NOT = { absolutism = 50 } }
			factor = 0.90
		}
		modifier = {
			owner = { absolutism = 60 }
			factor = 1.10
		}
		modifier = {
			owner = { absolutism = 70 }
			factor = 1.10
		}
		modifier = {
			owner = { absolutism = 80 }
			factor = 1.10
		}
		modifier = {
			owner = { absolutism = 90 }
			factor = 1.10
		}
		modifier = {
			owner = { absolutism = 100 }
			factor = 1.10
		}
		modifier = {
			any_neighbor_province = { has_province_modifier = mount_lebanon_emirate_disloyal }
			factor = 0.75
		}
		modifier = {
			any_neighbor_province = { has_province_modifier = mount_lebanon_emirate_rebellious }
			factor = 0.50
		}
	}
	
	option = {
		name = "flavor_syr.4.opta"
		if = {
			limit = {
				any_neighbor_province = { has_province_modifier = mount_lebanon_emirate_loyal }
			}
			add_permanent_province_modifier = {
				name = "mount_lebanon_emirate_loyal"
				duration = -1
			}
		}
		if = {
			limit = {
				any_neighbor_province = { has_province_modifier = mount_lebanon_emirate_disloyal }
			}
			add_permanent_province_modifier = {
				name = "mount_lebanon_emirate_disloyal"
				duration = -1
			}
		}
		if = {
			limit = {
				any_neighbor_province = { has_province_modifier = mount_lebanon_emirate_rebellious }
			}
			add_permanent_province_modifier = {
				name = "mount_lebanon_emirate_rebellious"
				duration = -1
			}
		}
	}
}

country_event = {
	id = flavor_syr.11
	title = "flavor_syr.11.name"
	desc = "flavor_syr.11.desc"
	picture = catholic_crusade_successful
	
	trigger = {
		any_owned_province = {
			has_province_modifier = mount_lebanon_emirate_loyal
		}
	}
	
	mean_time_to_happen = {
		years = 50
		
		modifier = {
			NOT = { stability = 1 }
			factor = 0.90
		}
		modifier = {
			NOT = { stability = 0 }
			factor = 0.90
		}
		modifier = {
			NOT = { stability = -1 }
			factor = 0.90
		}
		modifier = {
			NOT = { stability = -2 }
			factor = 0.90
		}
		modifier = {
			NOT = { stability = -3 }
			factor = 0.90
		}
		modifier = {
			has_real_disaster_trigger = yes
			factor = 0.10
		}
		modifier = {
			NOT = { absolutism = 10 }
			factor = 0.90
		}
		modifier = {
			NOT = { absolutism = 20 }
			factor = 0.90
		}
		modifier = {
			NOT = { absolutism = 30 }
			factor = 0.90
		}
		modifier = {
			NOT = { absolutism = 40 }
			factor = 0.90
		}
		modifier = {
			NOT = { absolutism = 50 }
			factor = 0.90
		}
		modifier = {
			absolutism = 60
			factor = 1.10
		}
		modifier = {
			absolutism = 70
			factor = 1.10
		}
		modifier = {
			absolutism = 80
			factor = 1.10
		}
		modifier = {
			absolutism = 90
			factor = 1.10
		}
		modifier = {
			absolutism = 100
			factor = 1.10
		}
	}
	
	option = {
		name = "flavor_syr.11.opta"
		every_owned_province = {
			limit = {
				has_province_modifier = mount_lebanon_emirate_loyal
			}
			remove_province_modifier = "mount_lebanon_emirate_loyal"
			add_permanent_province_modifier = {
				name = "mount_lebanon_emirate_disloyal"
				duration = -1
			}
		}
	}
}

country_event = {
	id = flavor_syr.21
	title = "flavor_syr.21.name"
	desc = "flavor_syr.21.desc"
	picture = catholic_crusade_successful
	
	trigger = {
		any_owned_province = {
			has_province_modifier = mount_lebanon_emirate_disloyal
		}
		OR = {
			has_real_disaster_trigger = yes
			is_at_war = yes
			is_bankrupt = yes
			is_in_deficit = yes
		}
	}
	
	mean_time_to_happen = {
		years = 50
		
		modifier = {
			has_real_disaster_trigger = yes
			factor = 0.10
		}
		modifier = {
			is_at_war = yes
			factor = 0.10
		}
		modifier = {
			is_bankrupt = yes
			factor = 0.10
		}
		modifier = {
			is_in_deficit = yes
			factor = 0.75
		}
		modifier = {
			NOT = { stability = 1 }
			factor = 0.90
		}
		modifier = {
			NOT = { stability = 0 }
			factor = 0.90
		}
		modifier = {
			NOT = { stability = -1 }
			factor = 0.90
		}
		modifier = {
			NOT = { stability = -2 }
			factor = 0.90
		}
		modifier = {
			NOT = { stability = -3 }
			factor = 0.90
		}
		modifier = {
			has_real_disaster_trigger = yes
			factor = 0.10
		}
		modifier = {
			NOT = { absolutism = 10 }
			factor = 0.90
		}
		modifier = {
			NOT = { absolutism = 20 }
			factor = 0.90
		}
		modifier = {
			NOT = { absolutism = 30 }
			factor = 0.90
		}
		modifier = {
			NOT = { absolutism = 40 }
			factor = 0.90
		}
		modifier = {
			NOT = { absolutism = 50 }
			factor = 0.90
		}
		modifier = {
			absolutism = 60
			factor = 1.10
		}
		modifier = {
			absolutism = 70
			factor = 1.10
		}
		modifier = {
			absolutism = 80
			factor = 1.10
		}
		modifier = {
			absolutism = 90
			factor = 1.10
		}
		modifier = {
			absolutism = 100
			factor = 1.10
		}
	}
	
	option = {
		name = "flavor_syr.21.opta"
		every_owned_province = {
			limit = {
				has_province_modifier = mount_lebanon_emirate_disloyal
			}
			remove_province_modifier = "mount_lebanon_emirate_disloyal"
			add_permanent_province_modifier = {
				name = "mount_lebanon_emirate_rebellious"
				duration = -1
			}
		}
	}
}

country_event = {
	id = flavor_syr.1001
	title = "flavor_syr.1001.name"
	desc = "flavor_syr.1001.desc"
	picture = muslim_jihad
	
	is_triggered_only = yes
	
	immediate = {
		if = {
			limit = {
				owns = 382 # Dimashq
				religion_group = muslim
			}
			custom_tooltip = lebanon_expedition_dimashq_tooltip
		}
		if = {
			limit = {
				owns = 377 # Halab
				NOT = { owns = 382 } # Dimashq
				religion_group = muslim
			}
			custom_tooltip = lebanon_expedition_halab_tooltip
		}
		if = {
			limit = {
				owns = 382 # Dimashq
				religion_group = christian
			}
			custom_tooltip = lebanon_expedition_damascus_tooltip
		}
		if = {
			limit = {
				owns = 377 # Halab
				NOT = { owns = 382 } # Dimashq
				religion_group = christian
			}
			custom_tooltip = lebanon_expedition_aleppo_tooltip
		}
		if = {
			limit = {
				NOT = { owns = 382 } # Dimashq
				NOT = { owns = 377 } # Halab
			}
			custom_tooltip = lebanon_expedition_courtier_tooltip
		}
	}
	
	option = {
		name = "flavor_syr.1001.opta"
		add_yearly_manpower = -0.05
		hidden_effect = {
			random_list = {
				75 = {
					country_event = { id = flavor_syr.1002 days = 30 }
				}
				25 = {
					country_event = { id = flavor_syr.1003 days = 30 }
				}
			}
		}
	}
}

country_event = {
	id = flavor_syr.1002
	title = "flavor_syr.1002.name"
	desc = "flavor_syr.1002.desc"
	picture = muslim_jihad_success
	
	is_triggered_only = yes
	
	immediate = {
		custom_tooltip = lebanon_expedition_population
		hidden_effect = {
			every_owned_province = {
				limit = {
					OR = {
						has_province_modifier = mount_lebanon_emirate_disloyal
						has_province_modifier = mount_lebanon_emirate_rebellious
					}
				}
				set_variable = { which = rural_percentage_removed   value = 0.1 }
				remove_rural_population_DEVS = yes
				set_variable = { which = urban_percentage_removed   value = 0.3 }
				remove_urban_population_DEVS = yes
				update_base_values = yes
			}
			1334 = {
				change_variable = { which = succesful_lebanon_exp value = 1 }
			}
		}
	}
	
	option = {
		name = "flavor_syr.1002.opta"
		clr_country_flag = expedition_under_way
		if = {
			limit = {
				1334 = {
					check_variable = { which = succesful_lebanon_exp value = 3 }
					OR = {
						AND = {
							NOT = { has_province_modifier = mount_lebanon_emirate_disloyal }
							NOT = { has_province_modifier = mount_lebanon_emirate_rebellious }
						}
						AND = {
							NOT = { check_variable = { which = druze value = 2 } }
							NOT = { check_variable = { which = chaldean value = 2 } }
						}
					}
				}
				2635 = {
					OR = {
						AND = {
							NOT = { has_province_modifier = mount_lebanon_emirate_disloyal }
							NOT = { has_province_modifier = mount_lebanon_emirate_rebellious }
						}
						AND = {
							NOT = { check_variable = { which = druze value = 2 } }
							NOT = { check_variable = { which = chaldean value = 2 } }
						}
					}
				}
				378 = {
					OR = {
						AND = {
							NOT = { has_province_modifier = mount_lebanon_emirate_disloyal }
							NOT = { has_province_modifier = mount_lebanon_emirate_rebellious }
						}
						AND = {
							NOT = { check_variable = { which = druze value = 2 } }
							NOT = { check_variable = { which = chaldean value = 2 } }
						}
					}
				}
			}
			every_owned_province = {
				limit = {
					OR = {
						has_province_modifier = mount_lebanon_emirate_disloyal
						has_province_modifier = mount_lebanon_emirate_rebellious
					}
				}
				remove_province_modifier = mount_lebanon_emirate_rebellious
				remove_province_modifier = mount_lebanon_emirate_disloyal
				hidden_effect = {
					1334 = {
						set_variable = { which = succesful_lebanon_exp value = 0 }
					}
				}
			}
		}
		else = {
			every_owned_province = {
				limit = {
					has_province_modifier = mount_lebanon_emirate_disloyal
				}
				remove_province_modifier = "mount_lebanon_emirate_disloyal"
				add_permanent_province_modifier = {
					name = "mount_lebanon_emirate_loyal"
					duration = -1
				}
			}
			every_owned_province = {
				limit = {
					has_province_modifier = mount_lebanon_emirate_rebellious
				}
				remove_province_modifier = "mount_lebanon_emirate_rebellious"
				add_permanent_province_modifier = {
					name = "mount_lebanon_emirate_disloyal"
					duration = -1
				}
			}
		}
	}
}

country_event = {
	id = flavor_syr.1003
	title = "flavor_syr.1003.name"
	desc = "flavor_syr.1003.desc"
	picture = muslim_jihad_failed
	
	is_triggered_only = yes
	
	immediate = {
		custom_tooltip = lebanon_expedition_population
		hidden_effect = {
			every_owned_province = {
				limit = {
					OR = {
						has_province_modifier = mount_lebanon_emirate_disloyal
						has_province_modifier = mount_lebanon_emirate_rebellious
					}
				}
				set_variable = { which = rural_percentage_removed   value = 0.1 }
				remove_rural_population_DEVS = yes
				set_variable = { which = urban_percentage_removed   value = 0.3 }
				remove_urban_population_DEVS = yes
				update_base_values = yes
			}
		}
	}
	
	option = {
		name = "flavor_syr.1003.opta"
		add_prestige = -5
		add_legitimacy = -5
		clr_country_flag = expedition_under_way
		set_country_flag = expedition_cooldown
	}
}
