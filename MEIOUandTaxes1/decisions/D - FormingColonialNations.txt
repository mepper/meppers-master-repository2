########################################
#                                      #
#      FormingColonialNations.txt      #
#                                      #
########################################
#
# Brazil
# Canada
# Chile
# Colombia
# Haiti
# La Plata
# Louisiana
# Mexico
# Paraguay
# Peru
# Quebec
# United States of America
# United Provinces of Central America
# Venezuela
#
########################################

country_decisions = {
	
	brazil_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = changed_from_colonial_nation }
			OR = {
				is_former_colonial_nation = yes
				is_colonial_nation = yes
			}
			colonial_brazil = {
				owned_by = ROOT
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
		}
		allow = {
			adm_tech = 10
			is_former_colonial_nation = yes
			is_free_or_tributary_trigger = yes
			is_at_war = no
			capital_scope = {
				colonial_region = colonial_brazil
				is_core = ROOT
			}
			num_of_owned_provinces_with = {
				colonial_region = colonial_brazil
				is_core = ROOT
				value = 6
			}
			NOT = { exists = BRZ }
		}
		effect = {
			change_tag = BRZ
			colonial_brazil = { limit = { owned_by = ROOT } remove_core = BRZ add_core = BRZ }
			colonial_brazil = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = BRZ }
			add_prestige = 25
			set_country_flag = changed_from_colonial_nation
			add_absolutism = 10
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = BRZ_ideas }
				}
				swap_national_ideas_effect = yes
			}
			#country_event = { id = governments.1 days = 5 }
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	canada_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = changed_from_colonial_nation }
			OR = {
				is_former_colonial_nation = yes
				is_colonial_nation = yes
			}
			colonial_canada = {
				owned_by = ROOT
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
		}
		allow = {
			adm_tech = 10
			is_former_colonial_nation = yes
			is_free_or_tributary_trigger = yes
			is_at_war = no
			capital_scope = {
				colonial_region = colonial_canada
				is_core = ROOT
			}
			num_of_owned_provinces_with = {
				colonial_region = colonial_canada
				is_core = ROOT
				value = 6
			}
			NOT = { exists = CAN }
		}
		effect = {
			change_tag = CAN
			colonial_canada = { limit = { owned_by = ROOT } remove_core = CAN add_core = CAN }
			colonial_canada = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = CAN }
			add_prestige = 25
			set_country_flag = changed_from_colonial_nation
			add_absolutism = 10
			#country_event = { id = governments.1 days = 5 }
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	chile_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = changed_from_colonial_nation }
			OR = {
				is_former_colonial_nation = yes
				is_colonial_nation = yes
			}
			colonial_chile = {
				owned_by = ROOT
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
		}
		allow = {
			adm_tech = 10
			is_former_colonial_nation = yes
			is_free_or_tributary_trigger = yes
			is_at_war = no
			NOT = { exists = CHL }
			capital_scope = {
				colonial_region = colonial_chile
				is_core = ROOT
			}
			num_of_owned_provinces_with = {
				colonial_region = colonial_chile
				is_core = ROOT
				value = 6
			}
		}
		effect = {
			change_tag = CHL
			colonial_chile = { limit = { owned_by = ROOT } remove_core = CHL add_core = CHL }
			colonial_chile = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = CHL }
			add_prestige = 25
			set_country_flag = changed_from_colonial_nation
			add_absolutism = 10
			#country_event = { id = governments.1 days = 5 }
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	colombia_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = changed_from_colonial_nation }
			OR = {
				is_former_colonial_nation = yes
				is_colonial_nation = yes
			}
			colonial_colombia = {
				owned_by = ROOT
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
		}
		allow = {
			adm_tech = 10
			is_former_colonial_nation = yes
			is_free_or_tributary_trigger = yes
			is_at_war = no
			capital_scope = {
				colonial_region = colonial_colombia
				is_core = ROOT
			}
			num_of_owned_provinces_with = {
				colonial_region = colonial_colombia
				is_core = ROOT
				value = 6
			}
			NOT = { exists = COL }
		}
		effect = {
			change_tag = COL
			colonial_colombia = { limit = { owned_by = ROOT } remove_core = COL add_core = COL }
			colonial_colombia = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = COL }
			add_prestige = 25
			set_country_flag = changed_from_colonial_nation
			add_absolutism = 10
			#country_event = { id = governments.1 days = 5 }
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	haiti_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = changed_from_colonial_nation }
			OR = {
				is_former_colonial_nation = yes
				is_colonial_nation = yes
			}
			colonial_hispanola = {
				owned_by = ROOT
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
		}
		allow = {
			adm_tech = 10
			is_former_colonial_nation = yes
			is_free_or_tributary_trigger = yes
			is_at_war = no
			capital_scope = {
				colonial_region = colonial_hispanola
				is_core = ROOT
			}
			num_of_owned_provinces_with = {
				colonial_region = colonial_hispanola
				is_core = ROOT
				value = 6
			}
			NOT = { exists = HAT }
		}
		effect = {
			change_tag = HAT
			colonial_hispanola = { limit = { owned_by = ROOT } remove_core = HAT add_core = HAT }
			colonial_hispanola = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = HAT }
			add_prestige = 25
			set_country_flag = changed_from_colonial_nation
			add_absolutism = 10
			#country_event = { id = governments.1 days = 5 }
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	la_plata_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = changed_from_colonial_nation }
			OR = {
				is_former_colonial_nation = yes
				is_colonial_nation = yes
			}
			colonial_la_plata = {
				owned_by = ROOT
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
		}
		allow = {
			adm_tech = 10
			is_former_colonial_nation = yes
			is_free_or_tributary_trigger = yes
			is_at_war = no
			capital_scope = {
				colonial_region = colonial_la_plata
				is_core = ROOT
			}
			num_of_owned_provinces_with = {
				colonial_region = colonial_la_plata
				is_core = ROOT
				value = 6
			}
			NOT = { exists = LAP }
		}
		effect = {
			change_tag = LAP
			colonial_la_plata = { limit = { owned_by = ROOT } remove_core = LAP add_core = LAP }
			colonial_la_plata = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = LAP }
			add_prestige = 25
			set_country_flag = changed_from_colonial_nation
			add_absolutism = 10
			#country_event = { id = governments.1 days = 5 }
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	louisiana_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = changed_from_colonial_nation }
			OR = {
				is_former_colonial_nation = yes
				is_colonial_nation = yes
			}
			colonial_louisiana = {
				owned_by = ROOT
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
		}
		allow = {
			adm_tech = 10
			is_former_colonial_nation = yes
			is_free_or_tributary_trigger = yes
			is_at_war = no
			capital_scope = {
				colonial_region = colonial_louisiana
				is_core = ROOT
			}
			num_of_owned_provinces_with = {
				colonial_region = colonial_louisiana
				is_core = ROOT
				value = 6
			}
			NOT = { exists = LOU }
		}
		effect = {
			change_tag = LOU
			colonial_louisiana = { limit = { owned_by = ROOT } remove_core = LOU add_core = LOU }
			colonial_louisiana = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = LOU }
			add_prestige = 25
			set_country_flag = changed_from_colonial_nation
			add_absolutism = 10
			#country_event = { id = governments.1 days = 5 }
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	mexico_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = changed_from_colonial_nation }
			OR = {
				is_former_colonial_nation = yes
				is_colonial_nation = yes
			}
			mexico_region = {
				owned_by = ROOT
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
		}
		allow = {
			adm_tech = 10
			is_former_colonial_nation = yes
			is_free_or_tributary_trigger = yes
			is_at_war = no
			capital_scope = {
				region = mexico_region
				is_core = ROOT
			}
			num_of_owned_provinces_with = {
				region = mexico_region
				is_core = ROOT
				value = 6
			}
			NOT = { exists = MEX }
		}
		effect = {
			change_tag = MEX
			mexico_region = { limit = { owned_by = ROOT } remove_core = MEX add_core = MEX }
			mexico_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = MEX }
			add_prestige = 25
			set_country_flag = changed_from_colonial_nation
			add_absolutism = 10
			#country_event = { id = governments.1 days = 5 }
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	paraguay_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = changed_from_colonial_nation }
			OR = {
				is_former_colonial_nation = yes
				is_colonial_nation = yes
			}
			colonial_la_plata = {
				owned_by = ROOT
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
		}
		allow = {
			adm_tech = 10
			is_former_colonial_nation = yes
			is_free_or_tributary_trigger = yes
			is_at_war = no
			NOT = { exists = PRG }
			owns_core_province = 775 # Guarani
			owns_core_province = 1486 # Piquiri
			capital_scope = {
				colonial_region = colonial_la_plata
				is_core = ROOT
			}
		}
		effect = {
			change_tag = PRG
			colonial_la_plata = { limit = { owned_by = ROOT } remove_core = PRG add_core = PRG }
			colonial_la_plata = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = PRG }
			add_prestige = 25
			set_country_flag = changed_from_colonial_nation
			add_absolutism = 10
			#country_event = { id = governments.1 days = 5 }
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	peru_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = changed_from_colonial_nation }
			OR = {
				is_former_colonial_nation = yes
				is_colonial_nation = yes
			}
			colonial_peru = {
				owned_by = ROOT
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
		}
		allow = {
			adm_tech = 10
			is_former_colonial_nation = yes
			is_free_or_tributary_trigger = yes
			is_at_war = no
			capital_scope = {
				colonial_region = colonial_peru
				is_core = ROOT
			}
			num_of_owned_provinces_with = {
				colonial_region = colonial_peru
				is_core = ROOT
				value = 6
			}
			NOT = { exists = PEU }
		}
		effect = {
			change_tag = PEU
			colonial_peru = { limit = { owned_by = ROOT } remove_core = PEU add_core = PEU }
			colonial_peru = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = PEU }
			add_prestige = 25
			set_country_flag = changed_from_colonial_nation
			add_absolutism = 10
			#country_event = { id = governments.1 days = 5 }
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	quebec_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = changed_from_colonial_nation }
			OR = {
				is_former_colonial_nation = yes
				is_colonial_nation = yes
			}
			colonial_canada = {
				owned_by = ROOT
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
		}
		allow = {
			adm_tech = 10
			is_former_colonial_nation = yes
			is_free_or_tributary_trigger = yes
			is_at_war = no
			NOT = { exists = QUE }
			capital_scope = {
				colonial_region = colonial_canada
				is_core = ROOT
			}
			num_of_owned_provinces_with = {
				colonial_region = colonial_canada
				is_core = ROOT
				value = 6
			}
		}
		effect = {
			change_tag = QUE
			colonial_canada = { limit = { owned_by = ROOT } remove_core = QUE add_core = QUE }
			colonial_canada = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = QUE }
			add_prestige = 25
			set_country_flag = changed_from_colonial_nation
			add_absolutism = 10
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = quebecois_ideas }
				}
				swap_national_ideas_effect = yes
			}
			#country_event = { id = governments.1 days = 5 }
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	upca_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = changed_from_colonial_nation }
			OR = {
				is_former_colonial_nation = yes
				is_colonial_nation = yes
			}
			central_america_region = {
				owned_by = ROOT
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
		}
		allow = {
			adm_tech = 10
			is_former_colonial_nation = yes
			is_free_or_tributary_trigger = yes
			is_at_war = no
			NOT = { exists = CAM }
			capital_scope = {
				region = central_america_region
				is_core = ROOT
			}
			num_of_owned_provinces_with = {
				region = central_america_region
				is_core = ROOT
				value = 6
			}
		}
		effect = {
			change_tag = CAM
			central_america_region = { limit = { owned_by = ROOT } remove_core = CAM add_core = CAM }
			central_america_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = CAM }
			add_prestige = 25
			set_country_flag = changed_from_colonial_nation
			add_absolutism = 10
			#country_event = { id = governments.1 days = 5 }
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	usa_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = changed_from_colonial_nation }
			OR = {
				is_former_colonial_nation = yes
				is_colonial_nation = yes
			}
			colonial_eastern_america = {
				owned_by = ROOT
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
		}
		allow = {
			adm_tech = 10
			is_former_colonial_nation = yes
			is_free_or_tributary_trigger = yes
			is_at_war = no
			capital_scope = {
				colonial_region = colonial_eastern_america
				is_core = ROOT
			}
			num_of_owned_provinces_with = {
				colonial_region = colonial_eastern_america
				is_core = ROOT
				value = 6
			}
			NOT = { exists = USA }
		}
		effect = {
			change_tag = USA
			colonial_eastern_america = { limit = { owned_by = ROOT } remove_core = USA add_core = USA }
			colonial_eastern_america = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = USA }
			add_prestige = 25
			set_country_flag = changed_from_colonial_nation
			add_absolutism = 10
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = USA_ideas }
				}
				swap_national_ideas_effect = yes
			}
			#country_event = { id = governments.1 days = 5 }
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	venezuela_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = changed_from_colonial_nation }
			OR = {
				is_former_colonial_nation = yes
				is_colonial_nation = yes
			}
			colonial_colombia = {
				owned_by = ROOT
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
		}
		allow = {
			adm_tech = 10
			is_former_colonial_nation = yes
			is_free_or_tributary_trigger = yes
			is_at_war = no
			NOT = { exists = VNZ }
			capital_scope = {
				colonial_region = colonial_colombia
				is_core = ROOT
			}
			num_of_owned_provinces_with = {
				colonial_region = colonial_colombia
				is_core = ROOT
				value = 6
			}
		}
		effect = {
			change_tag = VNZ
			colonial_colombia = { limit = { owned_by = ROOT } remove_core = VNZ add_core = VNZ }
			colonial_colombia = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = VNZ }
			add_prestige = 25
			set_country_flag = changed_from_colonial_nation
			add_absolutism = 10
			#country_event = { id = governments.1 days = 5 }
		}
		ai_will_do = {
			factor = 1
		}
	}
}

