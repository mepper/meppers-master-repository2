country_decisions = {
	imperial_administration = {
		potential = {
			always = no
			NOT = { has_country_modifier = "imperial_admin" }
			government = monarchy
			num_of_cities = 30
			is_free_or_tributary_trigger = yes
			prestige = 10
		}
		allow = {
			stability = 1
			num_of_cities = 45
			adm_power = 250
			prestige = 20
			is_at_war = no
		}
		effect = {
			add_country_modifier = {
				name = "imperial_admin"
				duration = -1
			}
			add_adm_power = -250
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 200	#FB
	}
	
	# Support for venetian_republic
	plutocratic_administration = {
		potential = {
			NOT = {
				OR = {
					government = merchant_republic
					government = venetian_republic
				}
			}
			NOT = { government = imperial_city }
			NOT = { government = merchant_imperial_city }
			OR = {
				technology_group = western
				technology_group = eastern
				technology_group = turkishtech
				technology_group = high_turkishtech
				technology_group = muslim
			}
			OR = {
				government = noble_republic
				government = administrative_republic
				government = republican_dictatorship
				government = oligarchic_republic
				tag = FRL
				tag = GEN
				tag = VEN
				tag = HSA
			}
			NOT = { num_of_provinces_in_states = 21 }
		}
		allow = {
			stability = 1
			full_idea_group = trade_ideas
			NOT = { num_of_provinces_in_states = 21 }
			is_free_or_tributary_trigger = yes
			num_of_ports = 1
			is_at_war = no
		}
		effect = {
			change_government = merchant_republic
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_4 has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_4 = yes
			}
			subtract_stability_2 = yes
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				num_of_cities = 3
			}
		}
	}
	
	changegov_altaic_despotic_monarchy = {
		potential = {
			government = tribal_nomads_altaic
		}
		allow = {
			adm_tech = 5			# From technology\ADM.txt
			stability = 1
			adm_power = 100			# From common\defines.lua
			NOT = { culture_group = altaic }
			NOT = { culture_group = tartar_group }
			NOT = { culture_group = tungusic }
			OR = {
				adm = 5
				mil = 5
			}
			is_at_war = no
		}
		effect = {
			add_adm_power = -100
			change_government = despotic_monarchy
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	changegov_despotic_monarchy = {
		potential = {
			OR = {
				government = feudal_monarchy
				government = administrative_monarchy
				government = absolute_monarchy
				government = english_monarchy
			}
		}
		allow = {
			adm_tech = 5			# From technology\ADM.txt
			absolutism = 30
			stability = 1
			adm_power = 100			# From common\defines.lua
			OR = {
				has_country_modifier = "arbitrary"
				has_country_modifier = "cruel"
				has_country_modifier = "flamboyant_schemer"
				has_country_modifier = "energetic"
				has_country_modifier = "selfish"
				adm = 5
				mil = 5
			}
			NOT = {
				OR = {
					tag = ITA
					tag = SPI
					tag = GER
					tag = ERG
				}
			}
			is_at_war = no
		}
		effect = {
			add_adm_power = -100
			change_government = despotic_monarchy
		}
		ai_will_do = {
			factor = 0		# You never want to willingly go back to despotic
		}
	}
	
	changegov_feudal_monarchy = {
		potential = {
			OR = {
				government = despotic_monarchy
				government = administrative_monarchy
				government = absolute_monarchy
				government = english_monarchy
			}
		}
		allow = {
			NOT = { absolutism = 25 }
			adm_tech = 5			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
			is_at_war = no
		}
		effect = {
			add_adm_power = -100
			# Tier 1
			change_government = feudal_monarchy
		}
		ai_will_do = {
			factor = 0		# You never want to willingly go back to feudal
		}
	}
	
	changegov_administrative_monarchy = {
		potential = {
			adm_tech = 18			# 50 years away
			OR = {
				government = despotic_monarchy
				government = medieval_monarchy
				government = feudal_monarchy
				government = constitutional_monarchy
				government = english_monarchy
			}
		}
		allow = {
			adm_tech = 23			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
			NOT = {
				OR = {
					tag = ITA
					tag = SPI
					tag = GER
					tag = ERG
				}
				#absolutism = 50
			}
			is_at_war = no
		}
		effect = {
			add_adm_power = -100
			change_government = administrative_monarchy
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0		# Don't go backwards from constitutional
				government = constitutional_monarchy
			}
			modifier = {
				factor = 0		# Don't go backwards from English
				government = english_monarchy
			}
		}
	}
	
	changegov_absolute_monarchy = {
		potential = {
			adm_tech = 30			# 50 years away
			OR = {
				government = despotic_monarchy
				government = medieval_monarchy
				government = feudal_monarchy
				government = administrative_monarchy
				government = enlightened_despotism
				government = english_monarchy
				government = portuguese_monarchy
			}
		}
		allow = {
			adm_tech = 35			# From technology\ADM.txt
			stability = 0
			adm = 3
			OR = {
				adm_power = 100			# From common\defines.lua
				AND = {
					government = administrative_monarchy
					adm_power = 250
				}
			}
			absolutism = 80
			is_at_war = no
		}
		effect = {
			# Higher cost for administrative, not a natural change.
			if = {
				limit = {
					government = administrative_monarchy
				}
				add_adm_power = -250
			}
			
			# Standard cost for rest
			if = {
				limit = {
					NOT = { government = administrative_monarchy }
				}
				add_adm_power = -100
			}
			change_government = absolute_monarchy
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0		# Don't go backwards from e.despotism
				government = enlightened_despotism
			}
			modifier = {
				factor = 0		# Don't go backwards from English
				government = english_monarchy
			}
		}
	}
	
	changegov_constitutional_monarchy = {
		potential = {
			adm_tech = 34			# 50 years away
			OR = {
				government = administrative_monarchy
				government = english_monarchy
				government = portuguese_monarchy
			}
		}
		allow = {
			adm_tech = 39			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
			is_at_war = no
		}
		effect = {
			add_adm_power = -100
			change_government = constitutional_monarchy
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				NOT = { absolutism = 60 }
			}
		}
	}
	
	changegov_enlightened_despotism = {
		potential = {
			adm_tech = 42			# 50 years away
			government = absolute_monarchy
		}
		allow = {
			adm_tech = 47			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
			is_at_war = no
		}
		effect = {
			add_adm_power = -100
			change_government = enlightened_despotism
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	changegov_religious_order = {
		potential = {
			government = theocratic_government
		}
		allow = {
			adm_tech = 7			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
			is_at_war = no
		}
		effect = {
			add_adm_power = -100
			change_government = monastic_order_government
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	changegov_theocracy = {
		potential = {
			government = monastic_order_government
		}
		allow = {
			adm_tech = 7			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
			is_at_war = no
		}
		effect = {
			add_adm_power = -100
			change_government = theocratic_government
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	changegov_medieval_monarchy = {
		potential = {
			government = medieval_monarchy
		}
		allow = {
			absolutism = 25
			adm_tech = 5			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
			is_at_war = no
		}
		effect = {
			add_adm_power = -100
			change_government = feudal_monarchy
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	changegov_eastern_thalassocracy = {
		potential = {
			government = eastern_thalassocracy
		}
		allow = {
			absolutism = 25
			adm_tech = 5			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
			is_at_war = no
		}
		effect = {
			add_adm_power = -100
			change_government = eastern_monarchy
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	changegov_administrative_republic = {
		potential = {
			OR = {
				government = oligarchic_republic
				government = noble_republic
				government = merchant_republic
			}
			adm_tech = 18
		}
		allow = {
			#absolutism = 50
			adm_tech = 23			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
			is_at_war = no
		}
		effect = {
			add_adm_power = -100
			change_government = administrative_republic
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				government = merchant_republic
			}
		}
	}
	
	changegov_noble_republic = {
		potential = {
			OR = {
				government = oligarchic_republic
				government = merchant_republic
			}
		}
		allow = {
			absolutism = 30
			adm_tech = 5			# From technology\ADM.txt
			stability = 1
			adm_power = 100			# From common\defines.lua
			faction_in_power = mr_aristocrats
			is_at_war = no
		}
		effect = {
			add_adm_power = -100
			subtract_stability_1 = yes
			change_government = noble_republic
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	changegov_oligarchic_republic = {
		potential = {
			government = noble_republic
		}
		allow = {
			absolutism = 20
			adm_tech = 5			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
			is_at_war = no
		}
		effect = {
			add_adm_power = -100
			change_government = oligarchic_republic
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	changegov_republican_dictatorship = {
		potential = {
			adm_tech = 29
			government = republic
			NOT = { government = imperial_city }
			NOT = { government = merchant_imperial_city }
			NOT = { government = republican_dictatorship }
		}
		allow = {
			absolutism = 70
			adm_tech = 34			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
			is_at_war = no
		}
		effect = {
			add_adm_power = -100
			change_government = republican_dictatorship
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	changegov_constitutional_republic = {
		potential = {
			adm_tech = 41
			government = republic
			NOT = { government = imperial_city }
			NOT = { government = merchant_imperial_city }
			NOT = { government = constitutional_republic }
		}
		allow = {
			absolutism = 70
			adm_tech = 46		# From technology\ADM.txt
			stability = 0
			adm_power = 100		# From common\defines.lua
			is_at_war = no
		}
		effect = {
			add_adm_power = -100
			change_government = constitutional_republic
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	changegov_asia_despotic_monarchy = {
		potential = {
			OR = {
				government = eastern_monarchy
				government = indian_monarchy
				government = rajput_monarchy
				government = maratha_confederacy
				chinese_gov_trigger = yes
			}
		}
		allow = {
			has_institution = enlightenment # technology_group = western
			adm_tech = 5			# From technology\ADM.txt
			stability = 1
			adm_power = 100			# From common\defines.lua
			OR = {
				adm = 5
				mil = 5
			}
			is_at_war = no
		}
		effect = {
			add_adm_power = -100
			change_government = despotic_monarchy
		}
		ai_will_do = {
			factor = 1
		}
	}
}
