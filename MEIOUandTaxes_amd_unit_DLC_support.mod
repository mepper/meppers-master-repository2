name="M&T American Dream DLC Support"
path="mod/MEIOUandTaxes_amd_unit_DLC_support"
dependencies={
	"MEIOU and Taxes 2.02"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesAR.jpg"
supported_version="1.24.*.*"
